package com.etech.recruitonmars.activities.dashboard

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etech.recruitonmars.R
import com.etech.recruitonmars.activities.login.LoginActivity
import com.etech.recruitonmars.base.BaseActivity
import com.etech.recruitonmars.databinding.ActivityHomeBinding
import com.etech.recruitonmars.activities.pointreward.PointsRewardedActivity
import com.etech.recruitonmars.activities.redemption.GetRedemptionActivity
import com.etech.recruitonmars.activities.rewardlist.RewardsListActivity
import kotlinx.coroutines.*
import java.net.URL
import java.util.concurrent.TimeUnit

class HomeActivity : BaseActivity() {
    lateinit var dashBoardViewModel: DashBoardViewModel
    lateinit var homeBinding: ActivityHomeBinding

    companion object {
        val TAG = "HomeActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dashBoardViewModel = ViewModelProvider(this).get(DashBoardViewModel::class.java)
        homeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        setUpViewModel(dashBoardViewModel)
        setUpview(homeBinding.extra,
            homeBinding.header,
            homeBinding.header.toolBar,
            "Home",
            true,
            true)
        val toggle = ActionBarDrawerToggle(
            this,
            homeBinding.drawerLayout,
            homeBinding.header.toolBar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        homeBinding.drawerLayout.addDrawerListener(toggle)
        toggle.drawerArrowDrawable.color = resources.getColor(R.color.app_background_color)
        toggle.syncState()
        init()

    }

    fun init() {
        Log.d(TAG, "Observe userdetail")
        var bitmap: Bitmap? = null
        dashBoardViewModel.getUserDetail()
        dashBoardViewModel.userDetail.observe(this, Observer { it ->
            Log.d(TAG, it.firstName + " " + it.lastName)
            homeBinding.userName.text = it.firstName + " " + it.lastName
            homeBinding.mobile.text = it.mobile
            CoroutineScope(Dispatchers.IO).launch {
                var url = URL(it.employerLogo)
                Log.d(TAG, "init: Url" + url)
                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                Log.d(TAG, "imagebitmap" + bitmap)
                Log.d(TAG, "init: io thread")
                delay(TimeUnit.SECONDS.toMillis(3))
                withContext(Dispatchers.Main)
                {
                    if (bitmap != null) {
                        homeBinding.userProfile.setImageBitmap(bitmap)
                    } else {
                        Log.d(TAG, "bitmap is null")
                    }
                    Log.d(TAG, "sendRequest() execute")
                }
                Log.d(TAG, "init: working on io")

            }


        })
        dashBoardViewModel.getRewardData()
        dashBoardViewModel.rewarsDetails.observe(this@HomeActivity, Observer { t ->
            Log.d(TAG, "initUi: " + t.balanceRewardPoint)
            Log.d(TAG, "initUi: " + t.pendingRedeemPoint)
            homeBinding.currentPointBalanceCircular.text = t.balanceRewardPoint
            homeBinding.pendingValue.text = t.pendingRedeemPoint
            Log.d(TAG, "initUi() called with: t = ${t.pendingRedeemPoint}")

            homeBinding.home.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    homeBinding.drawerLayout.closeDrawer(Gravity.LEFT, true)
                    dashBoardViewModel.getRewardData()

                }
            })

        })



        homeBinding.redemptions.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                redemptionClicked()
            }

        })
        homeBinding.pointsRewarded.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                pointRewardedClicked()
            }

        })
        homeBinding.rewardsList.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                var intent = Intent(this@HomeActivity, RewardsListActivity::class.java)
                startActivity(intent)
                homeBinding.drawerLayout.closeDrawer(Gravity.LEFT, true)

            }

        })
        homeBinding.logout.setOnClickListener(object : View.OnClickListener {

            override fun onClick(v: View?) {
                homeBinding.drawerLayout.closeDrawer(Gravity.LEFT, true)

                showAlert(this@HomeActivity,
                    "Recruit on MARS",
                    "Are you sure wnat to logout?",
                    "Ok",
                    object : View.OnClickListener {
                        override fun onClick(v: View?) {
                            dashBoardViewModel.logout()
                            var intent = Intent(this@HomeActivity, LoginActivity::class.java)
                            startActivity(intent)
                            finishAffinity()
                        }
                    },
                    "Cancle",
                    null)

            }


        })

        homeBinding.viewRedemption.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                redemptionClicked()
            }

        })
        homeBinding.viewPointRewarded.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                pointRewardedClicked()
            }

        })
    }
    fun redemptionClicked()
    {
        var intent=Intent(this@HomeActivity,GetRedemptionActivity::class.java)
        startActivity(intent)
        homeBinding.drawerLayout.closeDrawer(Gravity.LEFT,true)

    }
    fun pointRewardedClicked()
    {
        var intent=Intent(this@HomeActivity,PointsRewardedActivity::class.java)
        startActivity(intent)
        homeBinding.drawerLayout.closeDrawer(Gravity.LEFT,true)

    }
}