package com.etech.recruitonmars.activities.rewardlist

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.etech.recruitonmars.app.MyApplication
import com.etech.recruitonmars.base.BaseViewModel
import com.etech.recruitonmars.model.UserRewardList
import com.google.gson.Gson
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class RewardsListViewModel:BaseViewModel(){

    var rewardListLiveData=MutableLiveData<List<UserRewardList>>()
    var gson= Gson()
    private  val TAG = "PointRewardedViewModel"
    fun getRewardList() {



        startLoading()
        val employeeId = MyApplication.datamanger.getUserData().employeeId
        if (employeeId != null) {
            startLoading()
            MyApplication.datamanger.getRewardList(employeeId)
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(
                    Schedulers.io()).subscribe(
                    object : Observer<List<UserRewardList>> {
                        override fun onSubscribe(d: Disposable) {}
                        override fun onNext(response: List<UserRewardList>) {
                            Log.d(TAG, "onNext() called with: response = $response")
                            loadingSucess()
                               rewardListLiveData.value=response
                        }

                        override fun onError(e: Throwable) {
                            loadingFail(e.message)
                        }
                        override fun onComplete() {
                            Log.d(TAG, "onComplete() called")
                        }
                    })
        }


    }





}