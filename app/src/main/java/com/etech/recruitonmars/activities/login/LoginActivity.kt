package com.etech.recruitonmars.activities.login

import android.content.Intent
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.KeyEvent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etech.recruitonmars.R
import com.etech.recruitonmars.activities.dashboard.HomeActivity
import com.etech.recruitonmars.activities.forgotPassword.ForgotPassword
import com.etech.recruitonmars.base.BaseActivity
import com.etech.recruitonmars.utils.CustomAlert
import com.etech.recruitonmars.databinding.LoginActivityBinding

class LoginActivity : BaseActivity() {

    lateinit var viewModel: LoginViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        var loginActivityBinding: LoginActivityBinding
        super.onCreate(savedInstanceState)
        loginActivityBinding = DataBindingUtil.setContentView(this, R.layout.login_activity)
        viewModel= ViewModelProvider(this).get(LoginViewModel::class.java)
        setUpViewModel(viewModel)
        setUpview(loginActivityBinding.extra,
            loginActivityBinding.header,
            loginActivityBinding.header.toolBar,
            "Login",
            true,
            false)
        loginActivityBinding.edtLogin.addTextChangedListener(object : TextWatcher {
            var keyDial: Int = 0;

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                loginActivityBinding.edtLogin.setOnKeyListener(object : View.OnKeyListener {
                    @Override
                    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                        if (keyCode == KeyEvent.KEYCODE_DEL)
                            keyDial = 1
                        return false
                    }
                })

                if (keyDial == 0) {
                    val len: Int = loginActivityBinding.edtLogin.text.toString().length
                    if (len == 3 || len == 7) {


                        loginActivityBinding.edtLogin.text = Editable.Factory.getInstance()
                            .newEditable(loginActivityBinding.edtLogin.text.toString()+"-")
                        loginActivityBinding.edtLogin.setSelection(loginActivityBinding.edtLogin.text.toString().length)
                    }
                } else {
                    keyDial = 0
                }

            }

            override fun afterTextChanged(s: Editable?) {

            }
        })
        var ss = SpannableString(getString(R.string.text_forgot_password))
        var clickable=object :ClickableSpan(){
            override fun onClick(widget: View) {
                Log.d(TAG, "spannable textview clicked")
                val intent=Intent(this@LoginActivity,ForgotPassword::class.java)
                startActivity(intent)

            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText=true
                ds.underlineColor=ContextCompat.getColor(this@LoginActivity,R.color.colorPrimary)
                ds.linkColor=ContextCompat.getColor(this@LoginActivity,R.color.colorPrimary)
            }
        }
        ss.setSpan(clickable,0,10,Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
        ss.setSpan(ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorPrimary)),0,10,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        loginActivityBinding.clickable.text=ss
        loginActivityBinding.clickable.movementMethod=LinkMovementMethod.getInstance()
        loginActivityBinding.clickable.isEnabled=true


        loginActivityBinding.btnLogin.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                if(loginActivityBinding.edtLogin.text.toString().length!=12 && loginActivityBinding.edtPassword.text.toString().isNullOrEmpty())
                {
                    CustomAlert.showAlert(this@LoginActivity,getString(R.string.app_name),getString(R.string.alert_for_valid_login_detail))
                }
                else {
                    viewModel.doLogin(loginActivityBinding.edtLogin.text.toString(),
                        loginActivityBinding.edtPassword.text.toString())
                }
            }
        })
        observe()


    }

    private fun observe() {
     viewModel.userdata.observe(this, Observer { t->
           if(t.userDetails.userId!=null)
           {

               Log.d(TAG, "observe: "+t.userDetails.employerName)
               val intent= Intent(this, HomeActivity::class.java)
               startActivity(intent)
               finishAffinity()

           }
     })

    }
}

