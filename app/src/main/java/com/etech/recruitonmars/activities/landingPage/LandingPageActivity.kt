package com.etech.recruitonmars.activities.landingPage

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.etech.recruitonmars.R
import com.etech.recruitonmars.activities.dashboard.HomeActivity
import com.etech.recruitonmars.activities.login.LoginActivity
import com.etech.recruitonmars.base.BaseActivity
import com.etech.recruitonmars.utils.AppUtils
import kotlinx.coroutines.*
import java.util.concurrent.TimeUnit

class LandingPageActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)
        AppUtils.setStatusbarColor(this)
        var viewModel: LandingPageModel = ViewModelProvider(this).get(LandingPageModel::class.java)

        CoroutineScope(Dispatchers.IO).launch {
            delay(TimeUnit.SECONDS.toMillis(3))
            withContext(Dispatchers.Main)
            {

                setUpViewModel(viewModel)
                sendIntent();
                finish();
            }
        }
    }



    private fun sendIntent() {
        if(!isLoggedIn()) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        else
        {
            val intent=Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }

    }
}

