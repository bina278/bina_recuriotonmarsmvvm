package com.etech.recruitonmars.activities.redemption

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.etech.recruitonmars.R
import com.etech.recruitonmars.adapter.RedemptionAdapter
import com.etech.recruitonmars.app.MyApplication
import com.etech.recruitonmars.base.BaseFragment
import com.etech.recruitonmars.callback.OnRecylerViewClickListener
import com.etech.recruitonmars.databinding.RedemptionFragmentViewpagerBinding
import com.etech.recruitonmars.model.Redemption

 class RedemptionFragmentViewPager:BaseFragment() {

    private  val TAG = "RedemptionFragmentViewP"
    lateinit var redemptionbinding: RedemptionFragmentViewpagerBinding
    lateinit var redemptionViewModel:RedemptionViewModel
     var redemptionAdapter: RedemptionAdapter?=null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var status:String
      var mcontext:Context?=null


    companion object {
        private const val TAG = "RedemptionFragmentViewP"
         fun newInstance(page:Int):RedemptionFragmentViewPager
         {
             Log.d(TAG, "newInstance() called with: page = $page")
             
             var fragment=RedemptionFragmentViewPager()
             val bundle:Bundle?=Bundle()
             bundle!!.putInt("page",page)
             fragment.arguments=bundle
             return fragment

         }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mcontext=context
        if(context==null) {
            mcontext = MyApplication.appContext
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
            Log.d(TAG, "onCreate() called with: savedInstanceState = $savedInstanceState")
        super.onCreate(savedInstanceState)
             val page= arguments?.getInt("page")
             when(page){
                 0-> status="pending"
                 1-> status="approve"
                 2-> status="reject"
             }
    }




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        Log.d(TAG,
            "onCreateView() called with: inflater = $inflater, container = $container, savedInstanceState = $savedInstanceState")
        redemptionbinding = DataBindingUtil.inflate(inflater,
            R.layout.redemption_fragment_viewpager,
            container,
            false)

        return redemptionbinding.root


    }

     override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
         super.onViewCreated(view, savedInstanceState)
         redemptionViewModel= getDefaultViewModelProviderFactory().create(RedemptionViewModel::class.java)
         setUpViewModel(redemptionViewModel)
         Log.d(TAG,
             "onCreateView() called with: inflater ")
         redemptionViewModel.randomInt.observe(requireActivity(), Observer { t->
             Log.d(TAG, "onCreateView() called with:  randomInt = $t")
         })
         init()

     }

     fun init()
    {

        linearLayoutManager= LinearLayoutManager(mcontext)
        redemptionViewModel.getRedemption(status).observe(requireActivity(), Observer {

                t ->
              redemptionAdapter= RedemptionAdapter(mcontext!!,t,object: OnRecylerViewClickListener<Redemption>
            {
                override fun onLastItemReached() {
                }

                override fun onClick(position: Int, model: Redemption, view: View?, viewType: Int?) {
                }

            })
            redemptionbinding.recyclerView.addItemDecoration(DividerItemDecoration(requireContext(),
                DividerItemDecoration.VERTICAL))
            val itemDecoration= DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
            itemDecoration.setDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.recyclerview_divider)!!)
            redemptionbinding.recyclerView.adapter=redemptionAdapter
            redemptionAdapter!!.notifyDataSetChanged()
            redemptionbinding.recyclerView.layoutManager=linearLayoutManager


        })
    }


    override fun onPause() {
        Log.d(TAG, "onPause() called")
        super.onPause()
        redemptionViewModel= getDefaultViewModelProviderFactory().create(RedemptionViewModel::class.java)
        setUpViewModel(redemptionViewModel)

    }

    override fun onResume() {
        Log.d(TAG, "onResume() called")
        super.onResume()
        redemptionViewModel= getDefaultViewModelProviderFactory().create(RedemptionViewModel::class.java)
        setUpViewModel(redemptionViewModel)

    }

    override fun onDetach() {
        Log.d(TAG, "onDetach() called")
        super.onDetach()

    }
}








