package com.etech.recruitonmars.activities.rewardlist

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.etech.recruitonmars.R
import com.etech.recruitonmars.activities.reddemrequest.ReddemRequestActivity
import com.etech.recruitonmars.adapter.RewardListAdapter
import com.etech.recruitonmars.app.MyApplication
import com.etech.recruitonmars.base.BaseActivity
import com.etech.recruitonmars.callback.OnRecylerViewClickListener
import com.etech.recruitonmars.databinding.FragmentRewardsListBinding
import com.etech.recruitonmars.model.UserRewardList

class RewardsListActivity : BaseActivity() {
    private  val TAG = "RewardsListFragment"

    // TODO: Rename and change types of parameters
    lateinit var viewModel: RewardsListViewModel
    lateinit var binding: FragmentRewardsListBinding
    lateinit var rewardListAdapter: RewardListAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var totalPointAddedToCart=0
    var cartItemList=ArrayList<UserRewardList>()
    var rewardPoint=0
    var itemCount=0
    val onRequestSent=0

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_rewards_list)
        viewModel = ViewModelProvider(this).get(RewardsListViewModel::class.java)
        setUpview(binding.extra,binding.header,binding.header.toolBar,"Reward List",true,true)
        binding.header.cartImage.visibility=View.VISIBLE
        setUpViewModel(viewModel)
        init()

    }


fun init() {

    linearLayoutManager = LinearLayoutManager(this)
    viewModel.getRewardList()
    viewModel.rewardListLiveData.observe(this, Observer { t ->

        rewardPoint=(MyApplication.datamanger.getRewardData().balanceRewardPoint)!!.toInt()-(MyApplication.datamanger.getRewardData().pendingRedeemPoint)!!.toInt()
        rewardListAdapter =
            RewardListAdapter(this,
                t,
                object : OnRecylerViewClickListener<UserRewardList> {


                    override fun onClick(position: Int, model: UserRewardList, view: View?,viewType:Int?) {

                         if(viewType==OnRecylerViewClickListener.Btn_AddToCart) {
                             Log.d(TAG, "onClick: AddButton")

                             if (rewardPoint >= (model.rewardPoint)!!.toInt()) {
                                     totalPointAddedToCart =
                                         totalPointAddedToCart + model.rewardPoint!!.toInt()

                                     if (rewardPoint >= totalPointAddedToCart) {
                                         model.quantity = model.quantity + 1
                                         itemCount += 1
                                         binding.header.txtAddTocart.text = itemCount.toString()
                                         cartItemList.add(model)
                                         rewardListAdapter.notifyDataSetChanged();

                                     } else {
                                         totalPointAddedToCart =
                                             totalPointAddedToCart - model.rewardPoint!!.toInt()
                                         rewardListAdapter.notifyDataSetChanged();

                                         showAlert(this@RewardsListActivity,
                                             getString(R.string.app_name),
                                             getString(R.string.alert_for_reward_point))


                                     }


                                 if (itemCount > 0) {
                                     binding.header.cartImage.visibility=View.VISIBLE
                                     binding.header.txtAddTocart.visibility=View.VISIBLE
                                     binding.header.txtAddTocart.text = itemCount.toString()
                                 }

                             } else {
                                 showAlert(this@RewardsListActivity,
                                     getString(R.string.app_name)
                                     ,getString(R.string.alert_for_reward_point))

                             }
                         }

                        if(viewType==OnRecylerViewClickListener.Btn_Minus){
                            Log.d(TAG,
                                "onClick() btnMinus")

                       for(i in cartItemList)
                                {
                                    if(i.rewardId==model.rewardId)
                                    {
                                        totalPointAddedToCart = totalPointAddedToCart - model.rewardPoint!!.toInt()
                                        itemCount-=1
                                        binding.header.txtAddTocart.text = itemCount.toString()
                                        model.quantity=model.quantity-1
                                        Log.d(TAG, "onClick: btnMinus quantity"+model.quantity.toString())
                                        rewardListAdapter.notifyDataSetChanged();
                                        if(model.quantity==0)
                                        {
                                            cartItemList.remove(i)
                                        }
                                        break



                                    }

                                }
                                if(itemCount==0)
                                {
                                    binding.header.txtAddTocart.visibility=View.GONE

                                }
                            binding.header.txtAddTocart.visibility=View.VISIBLE
                            binding.header.txtAddTocart.text=itemCount.toString()
                            }

                        if(viewType==OnRecylerViewClickListener.Btn_Plus) {
                            Log.d(TAG,
                                "onClick() btnPlus")

                                        for (i in cartItemList) {
                                            if (i.rewardId == model.rewardId) {
                                                totalPointAddedToCart =
                                                    totalPointAddedToCart + model.rewardPoint!!.toInt()

                                                if (rewardPoint >= totalPointAddedToCart) {
                                                    model.quantity = model.quantity + 1
                                                    Log.d(TAG, "onClick: BtnPlus quantity "+model.quantity.toString())

                                                    itemCount += 1
                                                    binding.header.txtAddTocart.text = itemCount.toString()

                                                    rewardListAdapter.notifyDataSetChanged();

                                                } else {
                                                    totalPointAddedToCart =
                                                        totalPointAddedToCart - model.rewardPoint!!.toInt()
                                                    showAlert(this@RewardsListActivity,
                                                        getString(R.string.app_name),
                                                        getString(R.string.alert_for_reward_point))
                                                }
                                                break


                                            }
                                            binding.header.txtAddTocart.visibility = View.VISIBLE
                                            binding.header.txtAddTocart.text = itemCount.toString()


                                        }

                        }

                    }

                    override fun onLastItemReached() {
//                        TODO("Not yet implemented")
                    }
                })
        binding.recyclerView.addItemDecoration(DividerItemDecoration(this@RewardsListActivity,DividerItemDecoration.VERTICAL))
        var itemDecoration=DividerItemDecoration(this@RewardsListActivity,DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(ContextCompat.getDrawable(this@RewardsListActivity,R.drawable.recyclerview_divider)!!)

        binding.recyclerView.adapter = rewardListAdapter
        binding.recyclerView.layoutManager = linearLayoutManager


        binding.header.cartImage.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                if(itemCount==0)
                {
                    showAlert(this@RewardsListActivity,getString(R.string.app_name),"Please Select Item First")
                }
                else
                {
                    Log.d(TAG, "start reddemrequestActivity")
                        var intent= Intent(this@RewardsListActivity, ReddemRequestActivity::class.java)
                        intent.putExtra("Data",cartItemList)
                        startActivity(intent)
                        finishAffinity()

                }

            }

        })

    })




}
    override fun onBackPressed() {
        super.onBackPressed()
        binding.header.cartImage.visibility=View.GONE
    }
}