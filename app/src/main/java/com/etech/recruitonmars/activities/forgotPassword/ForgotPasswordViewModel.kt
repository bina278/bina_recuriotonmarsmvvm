package com.etech.recruitonmars.activities.forgotPassword

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.etech.recruitonmars.app.MyApplication
import com.etech.recruitonmars.base.BaseViewModel
import com.etech.recruitonmars.data.network.apiHelperNew.RestResponse
import com.etech.recruitonmars.model.UserData
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ForgotPasswordViewModel:BaseViewModel() {
    private  val TAG = "ForgotPasswordViewModel"
    var isSucess=MutableLiveData<String>()

    fun forgotPassword(phone:String)
    {
        startLoading()
        MyApplication.datamanger.forgotPassword(phone).observeOn(AndroidSchedulers.mainThread()).subscribeOn(
            Schedulers.io()).subscribe(
            object : Observer<String> {
                override fun onSubscribe(d: Disposable) {}
                override fun onNext(response: String) {
                    Log.d(TAG, "onNext() called with: response = $response")
                    loadingSucess()
                        Log.d(TAG, "onNext() called with: response = $response")

                         isSucess.value=response
                        Log.d(TAG, "response is sucessful")


                }


                override fun onError(e: Throwable) {
                    loadingFail(e.message)
                }
                override fun onComplete() {
                    Log.d(TAG, "onComplete() called")
                }
            })
    }



}