package com.etech.recruitonmars.activities.redemption

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.etech.recruitonmars.app.MyApplication
import com.etech.recruitonmars.base.BaseViewModel
import com.etech.recruitonmars.model.Redemption
import com.google.gson.Gson
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlin.random.Random

class RedemptionViewModel:BaseViewModel() {
    var randomInt= MutableLiveData<Int>()
    var liveDataRedemptionList=MutableLiveData<List<Redemption>>()


    var gson=Gson()
    private  val TAG = "RedemptionViewModel"
    fun getRedemption(status:String):MutableLiveData<List<Redemption>>{
        randomInt.value= Random(System.nanoTime()).nextInt()

        Log.d(TAG, "getRedemption() called with: status = $status")
        startLoading()
        val employerId= MyApplication.datamanger.getUserData().employerId
        val employeeId= MyApplication.datamanger.getUserData().employeeId
        if(employeeId!=null && employerId!=null) {
            MyApplication.datamanger.getRedempionList(employerId, employeeId,status)
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(
                    Schedulers.io()).subscribe(
                    object : Observer<List<Redemption>> {

                        override fun onSubscribe(d: Disposable) {}
                        override fun onNext(response: List<Redemption>) {
                            Log.d(TAG, "onNext() called with: response = $response")
                           liveDataRedemptionList.value=response
                            loadingSucess()
                            Log.d(TAG, "onNext() called with: response = $liveDataRedemptionList")

                        }

                        override fun onError(e: Throwable) {
                            loadingFail(e.message)
                            return
                        }
                        override fun onComplete() {
                            Log.d(TAG, "onComplete() called")
                        }
                    })
                }

        return liveDataRedemptionList

    }
}