package com.etech.recruitonmars.activities.forgotPassword

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etech.recruitonmars.R
import com.etech.recruitonmars.base.BaseActivity
import com.etech.recruitonmars.utils.CustomAlert
import com.etech.recruitonmars.databinding.ActivityForgotPasswordBinding

class ForgotPassword : BaseActivity() {
    lateinit var forgotPasswordBinding: ActivityForgotPasswordBinding
    lateinit var viewModel:ForgotPasswordViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate() called with: savedInstanceState = $savedInstanceState")
        super.onCreate(savedInstanceState)
        forgotPasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)
        viewModel= ViewModelProvider(this).get(ForgotPasswordViewModel::class.java)
        setUpViewModel(viewModel)
        setUpview(forgotPasswordBinding.extra,
            forgotPasswordBinding.header,
            forgotPasswordBinding.header.toolBar,
            "Forgot Password",
            true,
            true)
        forgotPasswordBinding.mobileNo.addTextChangedListener(object : TextWatcher {
            var keyDial: Int = 0;

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                forgotPasswordBinding.mobileNo.setOnKeyListener(object : View.OnKeyListener {
                    @Override
                    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                        if (keyCode == KeyEvent.KEYCODE_DEL)
                            keyDial = 1
                        return false
                    }
                })

                if (keyDial == 0) {
                    val len: Int = forgotPasswordBinding.mobileNo.text.toString().length
                    if (len == 3 || len == 7) {


                        forgotPasswordBinding.mobileNo.text = Editable.Factory.getInstance()
                            .newEditable((forgotPasswordBinding.mobileNo.text.toString()+"-"))
                        forgotPasswordBinding.mobileNo.setSelection(forgotPasswordBinding.mobileNo.text.toString().length)
                    }
                } else {
                    keyDial = 0
                }

            }

            override fun afterTextChanged(s: Editable?) {

            }
        })
        forgotPasswordBinding.btnSubmit.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                if(forgotPasswordBinding.mobileNo.text.toString().length!=12)
                {
                    CustomAlert.showAlert(this@ForgotPassword,getString(R.string.app_name),getString(R.string.alert_mobile_validation))
                }
                else {
                    viewModel.forgotPassword(forgotPasswordBinding.mobileNo.text.toString())

                }
            }

        })

        viewModel.isSucess.observe(this, Observer { msg->

            showAlert(this,getString(R.string.app_name),msg)
        })
    }
}