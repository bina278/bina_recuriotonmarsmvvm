package com.etech.recruitonmars.activities.pointreward

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.etech.recruitonmars.app.MyApplication
import com.etech.recruitonmars.base.BaseViewModel
import com.etech.recruitonmars.model.RewardData
import com.google.gson.Gson
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class PointRewardedViewModel:BaseViewModel() {
     var rewardData=MutableLiveData<RewardData>()




     var gson=Gson()
     private  val TAG = "PointRewardedViewModel"
     fun getRewardPoint(){
          startLoading()
          val employerId=MyApplication.datamanger.getUserData().employerId
          val employeeId=MyApplication.datamanger.getUserData().employeeId
          if(employeeId!=null && employerId!=null) {
               MyApplication.datamanger.getRewardPoint(employerId, employeeId)
                    .observeOn(AndroidSchedulers.mainThread()).subscribeOn(
                    Schedulers.io()).subscribe(
                    object : Observer<RewardData> {
                         override fun onSubscribe(d: Disposable) {}
                         override fun onNext(response: RewardData) {
                              Log.d(TAG, "onNext() called with: response = $response")
                              loadingSucess()

                                   rewardData.value = response
                              }


                         override fun onError(e: Throwable) {
                              loadingFail(e.message)
                         }
                         override fun onComplete() {
                              Log.d(TAG, "onComplete() called")
                         }
                    })
          }





     }


}