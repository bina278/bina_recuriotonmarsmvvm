package com.etech.recruitonmars.activities.reddemrequest

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.etech.recruitonmars.app.MyApplication
import com.etech.recruitonmars.base.BaseViewModel
import com.etech.recruitonmars.data.network.apiHelperNew.RestResponse
import com.etech.recruitonmars.model.RewardData
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import org.json.JSONObject

class ReddemRequestViewModel:BaseViewModel() {

    private val TAG = "ReddemRequestViewModel"
    var requestStatus=MutableLiveData<String>()
    fun getReddemRequest(items: JSONArray) {
        startLoading()
        val employerId = MyApplication.datamanger.getUserData().employerId
        val employeeId = MyApplication.datamanger.getUserData().employeeId
        if (employeeId != null && employerId != null) {
            MyApplication.datamanger.getReddemRequest(items,employerId, employeeId)
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(
                    Schedulers.io()).subscribe(
                    object : Observer<String> {
                        override fun onSubscribe(d: Disposable) {}
                        override fun onNext(response: String) {
                            Log.d(TAG, "onNext() called with: response = $response")
                            loadingSucess()

                                Log.d(TAG, "onNext() called with: response = $response")
                                requestStatus.value=response
                                MyApplication.datamanger.getUserRewardPoint(employerId,employeeId)

                        }

                        override fun onError(e: Throwable) {
                            loadingFail(e.message)
                        }
                        override fun onComplete() {
                            Log.d(TAG, "onComplete() called")
                        }
                    })
        }
    }
}








