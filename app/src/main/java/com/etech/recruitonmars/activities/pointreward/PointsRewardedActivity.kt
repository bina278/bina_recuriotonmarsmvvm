package com.etech.recruitonmars.activities.pointreward

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.etech.recruitonmars.R
import com.etech.recruitonmars.adapter.PointRewarsAdapter
import com.etech.recruitonmars.base.BaseActivity
import com.etech.recruitonmars.callback.OnRecylerViewClickListener
import com.etech.recruitonmars.utils.CustomAlert
import com.etech.recruitonmars.databinding.FragmentPointsRewardedBinding
import com.etech.recruitonmars.model.RewardList


class PointsRewardedActivity : BaseActivity() {
  lateinit var linearLayoutManager: LinearLayoutManager
  lateinit var pointRewardAdapter: PointRewarsAdapter
    lateinit var binding: FragmentPointsRewardedBinding
   lateinit var pointRewardedViewModel: PointRewardedViewModel


    private  val TAG = "PointsRewardedFragment"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this,R.layout.fragment_points_rewarded)
        setUpview(binding.extra,binding.header,binding.header.toolBar,"Point Rewarded",true,true)
        pointRewardedViewModel=ViewModelProvider(this).get(PointRewardedViewModel::class.java)
        setUpViewModel(pointRewardedViewModel)
        init()


    }



  fun init()
  {


      linearLayoutManager= LinearLayoutManager(this)
      pointRewardedViewModel.getRewardPoint()

      pointRewardedViewModel.rewardData.observe(this, Observer { t->
          binding.pointReward.visibility=View.VISIBLE
          binding.department.text=t.department
          binding.jobId.text=t.jobId
          binding.location.text=t.location
          binding.title.text=t.title
          binding.totalPoint.text=t.totalPoint

          pointRewardAdapter=PointRewarsAdapter(this,t.rewardsList,object: OnRecylerViewClickListener<RewardList>
          {


              override fun onLastItemReached() {
              }

              override fun onClick(position: Int, model: RewardList, view: View?, viewType: Int?) {
                  CustomAlert.showRewardPointDiolog(this@PointsRewardedActivity,model.rewardCode,model.rewardName,model.cFirstName+" "+model.cLastName,
                      model.rewardPoint,model.createDate)

              }


          })
          binding.recyclerView.addItemDecoration(DividerItemDecoration(this,
              DividerItemDecoration.VERTICAL))
          var itemDecoration= DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
          itemDecoration.setDrawable(ContextCompat.getDrawable(this,R.drawable.recyclerview_divider)!!)
          binding.recyclerView.adapter=pointRewardAdapter
          binding.recyclerView.layoutManager=linearLayoutManager

      })


  }
}