package com.etech.recruitonmars.activities.redemption

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.viewpager2.widget.ViewPager2
import com.etech.recruitonmars.R
import com.etech.recruitonmars.adapter.ViewPagerAdapter
import com.etech.recruitonmars.base.BaseActivity
import com.etech.recruitonmars.databinding.FragmentRedemptionBinding
import com.google.android.material.tabs.TabLayoutMediator

class GetRedemptionActivity : BaseActivity() {
    lateinit var binding: FragmentRedemptionBinding
    private  val TAG = "RedemptionFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_redemption)
        setUpview(binding.extra,binding.header,binding.header.toolBar,"Redemption",true,true)

        binding.viewPager.adapter= ViewPagerAdapter(this)
        TabLayoutMediator(binding.tabLayout,binding.viewPager){
                tab,position ->
            when(position){
                0-> tab.text="Pending"
                1-> tab.text="Approve"
                2-> tab.text="Reject"


            }
        }.attach()



        var callback=object : ViewPager2.OnPageChangeCallback(){
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int,
            ) {

                when (position) {
                    0 -> RedemptionFragmentViewPager.newInstance(0)
                    1 -> RedemptionFragmentViewPager.newInstance(1)
                    2 -> RedemptionFragmentViewPager.newInstance(2)
                }

            }

            override fun onPageSelected(position: Int) {


                when (position) {
                    0 -> RedemptionFragmentViewPager.newInstance(0)
                    1 -> RedemptionFragmentViewPager.newInstance(1)
                    2 -> RedemptionFragmentViewPager.newInstance(2)
                }

            }
            override fun onPageScrollStateChanged(state: Int) {
                super.onPageScrollStateChanged(state)
            }
        }
        binding.viewPager.registerOnPageChangeCallback(callback)


    }
}