package com.etech.recruitonmars.activities.reddemrequest

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.etech.recruitonmars.R
import com.etech.recruitonmars.activities.dashboard.HomeActivity
import com.etech.recruitonmars.activities.rewardlist.RewardsListActivity
import com.etech.recruitonmars.adapter.RedeemRequestAdapter
import com.etech.recruitonmars.app.MyApplication
import com.etech.recruitonmars.base.BaseActivity
import com.etech.recruitonmars.callback.OnRecylerViewClickListener
import com.etech.recruitonmars.databinding.ActivityReddemRequestBinding
import com.etech.recruitonmars.model.UserRewardList
import com.etech.recruitonmars.utils.CustomAlert
import org.json.JSONArray
import org.json.JSONObject

class ReddemRequestActivity : BaseActivity() {
   lateinit var viewModel:ReddemRequestViewModel
   lateinit var binding:ActivityReddemRequestBinding
   lateinit var requestItemAdapter:RedeemRequestAdapter
   lateinit var linearLayoutManager:LinearLayoutManager
   lateinit var localBroadcastManager:LocalBroadcastManager
   var rewarddataList=ArrayList<UserRewardList>()
   var itemInCart=0
    var totalPointAddedToCart=0
   override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_reddem_request)
        setUpview(binding.extra,binding.header,binding.header.toolBar,"Add To Cart",true,true)
        viewModel= ViewModelProvider(this).get(ReddemRequestViewModel::class.java)
        setUpViewModel(viewModel)
        rewarddataList= intent.getSerializableExtra("Data") as ArrayList<UserRewardList>
        for(i in rewarddataList){
            itemInCart = itemInCart+i.quantity
            totalPointAddedToCart=totalPointAddedToCart+( (i.quantity)*(i.rewardPoint!!.toInt()))

        }
       binding.btnRequest.setOnClickListener(object:View.OnClickListener {
           override fun onClick(v: View?) {
               var jsonToSend= JSONArray()
               for (i in rewarddataList) {
                   var jsonObject=JSONObject()
                   jsonObject.put("item_id", i.rewardId)
                   jsonObject.put("item_quantity", i.quantity)
                   jsonToSend.put(jsonObject)
                   Log.d(TAG, "onClick: " + jsonToSend.toString())

               }
               viewModel.getReddemRequest(jsonToSend)


           }
       })

       init()


   }
    fun init()
    {


        var rewardPoint=MyApplication.datamanger.getRewardData().balanceRewardPoint!!.toInt()-MyApplication.datamanger.getRewardData().pendingRedeemPoint!!.toInt()
        linearLayoutManager=LinearLayoutManager(this)
        requestItemAdapter=
            RedeemRequestAdapter(this,rewarddataList,object: OnRecylerViewClickListener<UserRewardList>
            {
                override fun onLastItemReached() {
                }
                override fun onClick(position: Int, model: UserRewardList, view: View?,viewType:Int?) {
                    if (viewType == OnRecylerViewClickListener.Btn_Minus) {


                            for(i in rewarddataList)
                            {
                                if(i.rewardId==model.rewardId)
                                {
                                    totalPointAddedToCart = totalPointAddedToCart - model.rewardPoint!!.toInt()
                                    model.quantity=model.quantity-1
                                    binding.requestRewardPoint.text=totalPointAddedToCart.toString()
                                    binding.remainingPoint.text=(rewardPoint-totalPointAddedToCart).toString()
                                    requestItemAdapter.notifyDataSetChanged()
                                    itemInCart=itemInCart-1
                                    if(model.quantity==0)
                                    {
                                        rewarddataList.remove(model)
                                        requestItemAdapter.notifyDataSetChanged()
                                    }
                                    break
                                }

                            }


                    }
                    if (viewType == OnRecylerViewClickListener.Btn_Plus) {
                            for(i in rewarddataList)
                            {
                                if(i.rewardId==model.rewardId) {
                                    totalPointAddedToCart =totalPointAddedToCart + model.rewardPoint!!.toInt()

                                    if (rewardPoint >= totalPointAddedToCart) {
                                        model.quantity=model.quantity + 1
                                        itemInCart=itemInCart+1
                                        binding.requestRewardPoint.text=totalPointAddedToCart.toString()
                                        binding.remainingPoint.text=(rewardPoint-totalPointAddedToCart).toString()
                                        requestItemAdapter.notifyDataSetChanged()


                                    } else {
                                        totalPointAddedToCart =
                                            totalPointAddedToCart - model.rewardPoint!!.toInt()
                                        showAlert(this@ReddemRequestActivity,getString(R.string.app_name),getString(R.string.alert_for_reward_point))
                                    }
                                    break
                                }
                            }

                    }
                }
          })
         binding.currentPointBalance.text=(rewardPoint).toString()
         binding.requestRewardPoint.text=totalPointAddedToCart.toString()
         binding.remainingPoint.text=(rewardPoint-totalPointAddedToCart).toString()



        binding.recyclerView.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))
        var itemDecoration= DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(ContextCompat.getDrawable(this,R.drawable.recyclerview_divider)!!)
        binding.recyclerView.adapter=requestItemAdapter
        binding.recyclerView.layoutManager=linearLayoutManager


         localBroadcastManager= LocalBroadcastManager.getInstance(this)

          viewModel.requestStatus.observe(this, Observer{t->
                   CustomAlert.showAlert(this,getString(R.string.app_name),t,null,null,null,null,"Ok",object :View.OnClickListener{
                       override fun onClick(v: View?) {
                           val intent=Intent(this@ReddemRequestActivity,HomeActivity::class.java)
                           startActivity(intent)
                           finishAffinity()


                       }

                   })
                    rewarddataList.clear()
                    requestItemAdapter.notifyDataSetChanged()
          })


    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent=Intent(this,RewardsListActivity::class.java)
        startActivity(intent)
    }
}







