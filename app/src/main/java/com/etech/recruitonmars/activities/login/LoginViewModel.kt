package com.etech.recruitonmars.activities.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.etech.recruitonmars.app.MyApplication
import com.etech.recruitonmars.base.BaseViewModel
import com.etech.recruitonmars.data.network.apiHelperNew.RestResponse
import com.etech.recruitonmars.model.UserData
import com.google.gson.Gson
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class LoginViewModel:BaseViewModel() {
    private  val TAG = "LoginViewModel"
    var gson= Gson()
    var userdata=MutableLiveData<UserData>()
    fun doLogin(mobile:String,password:String)
    {
        startLoading()
        Log.d(TAG, "doLogin() called with: mobile = $mobile, password = $password")
        MyApplication.datamanger.doLogin(mobile, password).observeOn(AndroidSchedulers.mainThread()).subscribeOn(
            Schedulers.io()).subscribe(
            object : Observer<UserData> {
                override fun onSubscribe(d: Disposable) {}
                override fun onNext(userData: UserData) {
                    Log.d(TAG, "onNext() called with: response = $userData")
                   loadingSucess()
                        Log.d(TAG, "onNext() called with: response = $userData")

                        userdata.value=userData
//                        isLogin.value=true
                        Log.d(TAG, "response is sucessful")
                        Log.d(TAG, MyApplication.datamanger.getRewardData().pendingRedeemPoint+MyApplication.datamanger.getRewardData().balanceRewardPoint)
                        Log.d(TAG, "user data"+MyApplication.datamanger.getUserData().firstName)


                }

                override fun onError(e: Throwable) {
                    loadingFail(e.message)
                }
                override fun onComplete() {
                    Log.d(TAG, "onComplete() called")
                }
            })

//        Log.d(TAG, "doLogin: "+userdetails)
    }









}