package com.etech.recruitonmars.model

import com.google.gson.annotations.SerializedName

class RewardData {

    @SerializedName("job_id")
    var jobId:String?=null

    @SerializedName("title")
    var title:String?=null

    @SerializedName("department")
    var department:String?=null

    @SerializedName("location")
    var location:String?=null

    @SerializedName("total_point")
    var totalPoint:String?=null

    @SerializedName("rewards")
     lateinit var rewardsList:List<RewardList>








}