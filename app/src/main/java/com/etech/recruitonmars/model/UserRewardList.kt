package com.etech.recruitonmars.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UserRewardList:Serializable{

    @SerializedName("reward_name")
    var rewardName:String?=null

    @SerializedName("reward_id")
    var rewardId:String?=null

    @SerializedName("reward_point")
    var rewardPoint:String?=null

    @SerializedName("max_amount")
    var maxAmount:String?=null

    @SerializedName("create_date")
    var createDate:String?=null

    @SerializedName("redeem_manager")
    var redeemManager:String?=null

    var quantity:Int=0



}