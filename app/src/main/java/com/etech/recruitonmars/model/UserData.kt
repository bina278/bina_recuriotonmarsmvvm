package com.etech.recruitonmars.model

import android.net.Uri
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.net.URL

class UserData:Serializable{
  @SerializedName("user_detail")
  lateinit var userDetails: Userdetails

  @SerializedName("reward_detail")
  lateinit var rewardDetail: RewardDetail
}

  class Userdetails {

    //    {"res_code":1,"res_message":"Login Success","res_object":{"user_detail":{"user_id":"3731","user_status":"active","email":"bhavik@gmail.com",
//            "password":"0cc175b9c0f1b6a831c399e269772661","user_type":"2","employer_id":"1","employee_id":"3712","employer_name":"eTechMavens","
//        employer_status":"active","first_name":"Bhavik","last_name":"Patel","mobile":"7878565599",
//        "employer_logo":"http:\/\/s3.amazonaws.com\/ers-uploads\/employer\/583d2fc6c5619.jpg"},
//        "reward_detail":{"balance_reward_point":"256","pending_redeem_point":"256"}}}

    @SerializedName("user_id")
     var userId: String? =null;

    @SerializedName("user_status")
     var userStatus: String? =null;

    @SerializedName("email")
      var email: String? =null;

    @SerializedName("password")
      var password: String? =null;

    @SerializedName("user_type")
      var userType: String? = null;

    @SerializedName("employer_id")
     var employerId: String? =null;

    @SerializedName("employee_id")
      var employeeId: String? = null

    @SerializedName("employer_name")
      var employerName: String? =null

    @SerializedName("employer_status")
      var employerStatus: String? = null

    @SerializedName("first_name")
     var firstName: String? =null

    @SerializedName("last_name")
      var lastName: String? =null

    @SerializedName("mobile")
      var mobile: String? =null

    @SerializedName("employer_logo")
     var employerLogo: String? = null;
  }


class RewardDetail {
    @SerializedName("balance_reward_point")
      var balanceRewardPoint: String? = null

    @SerializedName("pending_redeem_point")
      var pendingRedeemPoint: String? =null


  }
