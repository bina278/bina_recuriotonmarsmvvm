package com.etech.recruitonmars.model

import com.google.gson.annotations.SerializedName

class RewardList {

    @SerializedName("reward_id")
    var rewardId:String?=null


    @SerializedName("reward_name")
    var rewardName:String?=null


    @SerializedName("reward_type")
    var rewardType:String?=null


    @SerializedName("reward_code")
    var rewardCode:String?=" "


    @SerializedName("create_date")
    var createDate:String?=null


    @SerializedName("reward_point")
    var rewardPoint:String?=null


    @SerializedName("candidate_first_name")
    var cFirstName:String?=null


    @SerializedName("candidate_last_name")
    var cLastName:String?=null










}