package com.etech.recruitonmars.model

import com.google.gson.annotations.SerializedName

class Redemption {

    @SerializedName("reward")
    var reward:String?=null

    @SerializedName("reddem_point")
    var redeemPoint:String?=null

}