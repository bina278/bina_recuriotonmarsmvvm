package com.etech.recruitonmars.data.network

import com.etech.recruitonmars.data.network.apiHelperNew.RestResponse
import com.etech.recruitonmars.model.*
import io.reactivex.Observable
import org.json.JSONArray
import java.util.*

interface  ApiHelper {

  fun doLogin(mobile:String,password:String):Observable<UserData>

  fun getRewardPoint(employerId:String,employeeId:String):Observable<RewardData>

  fun getRewardList(employeeId: String):Observable<List<UserRewardList>>

  fun getRedempionList(employerId:String,employeeId:String,status:String):Observable<List<Redemption>>

  fun forgotPassword(phone:String):Observable<String>

  fun getReddemRequest(jsonArray: JSONArray?,employerId:String,employeeId:String):Observable<String>

  fun getUserRewardPoint(employerId:String,employeeId:String):Observable<RewardDetail>





}