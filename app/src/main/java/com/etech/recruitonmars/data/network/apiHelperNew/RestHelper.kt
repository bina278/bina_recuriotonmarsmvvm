package com.etech.recruitonmars.data.network.apiHelperNew

import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import com.etech.recruitonmars.utils.AppUtils
import com.etech.recruitonmars.utils.Constants
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.*
import org.json.JSONObject
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.log


@Suppress("unused")
/**
 * Created by etech10 on 10/8/17.
 */
class RestHelper private constructor(restRequest: RestRequest, private val callback: RestHelperCallback) :
    RestClient.RestClientListener {
    private val restClient: RestClient = RestClient(restRequest, this)
    private  val TAG = "RestHelper"
    interface RestHelperCallback {
        fun onRequestCallback(code: Int, message: String, restResponse: RestResponse)
    }


    class Builder {
        private var baseUrl = DEFAULT_BASE_URL
        private var requestMethod: RestConst.RequestMethod = RestConst.RequestMethod.METHOD_POST
        private var contentType: RestConst.ContentType = RestConst.ContentType.CONTENT_FORMDATA
        private var url: String? = null
        private var params: HashMap<String, String>? = null
        private var headers: HashMap<String, String>? = null
        private var attachments: HashMap<String, List<String>>? = null
        private var jsonParams: JSONObject? = null
        private var callback: RestHelperCallback = object : RestHelperCallback {
            override fun onRequestCallback(code: Int, message: String, restResponse: RestResponse) {

            }
        }

        fun setBaseUrl(baseUrl: String): Builder{
            this.baseUrl = baseUrl
            return this
        }

        fun setRequestMethod(requestMethod: RestConst.RequestMethod): Builder{
            this.requestMethod = requestMethod
            return this
        }

        fun setContentType(contentType: RestConst.ContentType): Builder {
            this.contentType = contentType
            return this
        }

        fun setUrl(url: String): Builder {
            this.url = url
            return this
        }

        fun setHeaders(headers: HashMap<String, String>): Builder {
            this.headers = headers
            return this
        }

        fun setParams(params: HashMap<String, String>): Builder {
            this.params = params
            return this
        }

        fun setAttachments(attachments: HashMap<String, List<String>>): Builder {
            this.attachments = attachments
            return this
        }

        fun setJsonObject(jsonObject: JSONObject): Builder {
            this.jsonParams = jsonObject
            return this
        }

        fun setCallBack(callBack: RestHelperCallback): Builder {
            this.callback = callBack
            return this
        }

        fun build(): RestHelper {
            if (TextUtils.isEmpty(baseUrl) || TextUtils.isEmpty(url)) {
                throw IllegalArgumentException("baseurl and url should not be empty")
            }
            return RestHelper(
                RestRequest(
                    requestMethod,
                    contentType,
                    baseUrl!!,
                    url!!,
                    headers,
                    params,
                    attachments,
                    jsonParams
                ), callback
            )
        }

    }

//    fun sendRequest() {
//        restClient.execute()
//    }

    fun cancelRequest() {
        restClient.cancelRequest()
    }

   override fun onRequestComplete(responseCode: RestConst.ResponseCode, restResponse: RestResponse) {
       Log.d(TAG,
           "onRequestComplete() called with: responseCode = $responseCode, restResponse = $restResponse")
        if (responseCode == RestConst.ResponseCode.SUCCESS) {
            var jsonObject: JSONObject? = null
            var message = ""
            var resCode = 0
            try {
                jsonObject = JSONObject(restResponse.resString)
                if (jsonObject != null) {
                    if (jsonObject.has(Constants.RES_CODE_KEY)) {
                        resCode = jsonObject.getInt(Constants.RES_CODE_KEY)
                    }
                }
                if (jsonObject != null) {
                    if (jsonObject.has(Constants.RES_MSG_KEY)) {
                        message = jsonObject.getString(Constants.RES_MSG_KEY)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            if (jsonObject != null && jsonObject.has(Constants.RES_CODE_KEY)) {
                if (resCode == Constants.SUCCESS_CODE) {
                    restResponse.code=Constants.SUCCESS_CODE
                    restResponse.message=message
                    restResponseObservable!!.onNext(restResponse)
//                    callback.onRequestCallback(Constants.SUCCESS_CODE, message, restResponse)
                } else {
                    restResponse.code=Constants.FAIL_CODE
                    restResponse.message=message
                    restResponseObservable!!.onNext(restResponse)

//                    callback.onRequestCallback(Constants.FAIL_CODE, message, restResponse)
                }
            } else {
                Log.d(TAG,
                    "onRequestComplete() called with: responseCode = $responseCode, restResponse = $restResponse")
                restResponse.code=Constants.FAIL_CODE
                restResponse.message= INTERNAL_SERVER_ERR
                restResponseObservable!!.onNext(restResponse)

//                callback.onRequestCallback(Constants.FAIL_CODE, INTERNAL_SERVER_ERR, restResponse)
            }
        } else {
            if (responseCode == RestConst.ResponseCode.CANCEL) {
                Log.d(TAG,
                    "onRequestComplete() called with: responseCode = $responseCode, restResponse = $restResponse")
                restResponse.code=Constants.CANCEL_CODE
                restResponse.message= INTERNAL_SERVER_ERR
                restResponseObservable!!.onNext(restResponse)
//                callback.onRequestCallback(Constants.CANCEL_CODE, INTERNAL_SERVER_ERR, restResponse)
            } else if (responseCode == RestConst.ResponseCode.ERROR && !AppUtils.isConnectingToInternet()) {
                Log.d(TAG,
                    "onRequestComplete() called with: responseCode = $responseCode, restResponse = $restResponse")
                restResponse.code=Constants.FAIL_INTERNET_CODE
                    restResponse.message= NO_INTERNET_ERR
                restResponseObservable!!.onNext(restResponse)

                //                callback.onRequestCallback(Constants.FAIL_INTERNET_CODE, NO_INTERNET_ERR, restResponse)
            } else {
//
            //                callback.onRequestCallback(Constants.FAIL_CODE, INTERNAL_SERVER_ERR, restResponse)

                Log.d(TAG,
                    "onRequestComplete() called with: responseCode = $responseCode, restResponse = $restResponse")
              restResponse.code=Constants.FAIL_CODE
                restResponse.message= INTERNAL_SERVER_ERR
                restResponseObservable!!.onNext(restResponse)


            }
        }

    }

    companion object {
        //todo set this in constructor of AppApiHelper
        var DEFAULT_BASE_URL: String? = null

        private const val INTERNAL_SERVER_ERR = "Internal Server Error. Please try again later."
        private const val NO_INTERNET_ERR = "No internet connection"
    }

   lateinit var restResponseObservable: PublishSubject<RestResponse>
    fun sendRequest(): PublishSubject<RestResponse> {
//        var restResponseObservable: PublishSubject<RestResponse<V>>? = null
        Log.d(TAG, "sendRequest() called")
        restResponseObservable = PublishSubject.create()

        CoroutineScope(Dispatchers.IO).launch {
            delay(TimeUnit.SECONDS.toMillis(3))
            withContext(Dispatchers.Main)
            {
                restClient.execute()
                Log.d(TAG, "sendRequest() execute")
            }
        }
        Log.d(TAG, "sendRequest() finished")
        return restResponseObservable



    }

}



