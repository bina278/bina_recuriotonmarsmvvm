package com.etech.recruitonmars.data.network.apiHelperNew

import org.json.JSONObject


@Suppress("unused")
/**
 * Created by etech7 on 19/6/17.
 */

class RestRequest(
    val reqMethod: RestConst.RequestMethod,
    val contentType: RestConst.ContentType,
    val baseUrl: String,
    val reqUrl: String,
    val header: MutableMap<String, String>?,
    val params: MutableMap<String, String>?,
    val attachments: MutableMap<String, List<String>>?,
    val jsonObject: JSONObject?
) {

    @Suppress("PrivatePropertyName")
    private val TAG = "RestRequest"

    //addParam Method Use for Set Parameter


    override fun toString(): String {
        val stringBuilder = StringBuilder("RestRequest")
        stringBuilder.append("\n reqMethod=")
        if (reqMethod == RestConst.RequestMethod.METHOD_GET) {
            stringBuilder.append("get")
        } else {
            stringBuilder.append("post").append("\n content type=")

            when (contentType) {
                RestConst.ContentType.CONTENT_JSON -> {
                    stringBuilder.append("json")
                    if (jsonObject != null) {
                        stringBuilder.append("\n JSONObject=").append(jsonObject.toString())
                    }
                }
                RestConst.ContentType.CONTENT_FORMDATA -> stringBuilder.append("formdata")
                RestConst.ContentType.CONTENT_MULTIPART -> stringBuilder.append("multipart")
            }
        }
        stringBuilder.append("\nbaseurl=").append(baseUrl)
            .append("\n url").append(reqUrl)
        if (header != null) {
            stringBuilder.append("\n headers")
            for ((key, value) in header) {
                stringBuilder.append("\n").append(key).append(":")
                    .append(value)
            }
        }

        if (params != null) {
            stringBuilder.append("\n params")
            for ((key, value) in params) {
                stringBuilder.append("\n").append(key).append(":")
                    .append(value)
            }
        }
        if (attachments != null) {
            stringBuilder.append("\n attachments")
            for ((key, value) in attachments) {
                stringBuilder.append("\n").append(key).append(":")
                for (files in value) {
                    stringBuilder.append("\n ").append(files)
                }
            }
        }
        return stringBuilder.toString()
    }
}
