package com.etech.recruitonmars.data.network

import android.annotation.SuppressLint
import android.util.Log
import com.etech.recruitonmars.data.network.apiHelperNew.RestHelper
import com.etech.recruitonmars.data.network.apiHelperNew.RestResponse
import com.etech.recruitonmars.model.*
import com.etech.recruitonmars.utils.AppUtils
import com.etech.recruitonmars.utils.Constants
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class AppApihelper:ApiHelper {
    private  val TAG = "AppApihelper"
    companion object{
        const  val BASE_URL = "http://ec2-54-226-115-83.compute-1.amazonaws.com/sd/app/api/"
        const  val API_LOGIN_URL = "User_Controller/login"
        const val API_REWARD_POINT="Reward_Controller/getEmployeeReward"
        const val API_REWARD_LIST="Reward_Controller/getRewardList"
        const val API_REDEMPTION="Reward_Controller/getEmployeeReddemption"
        const val API_FORGOT_PASSWORD="User_Controller/forgotPassword"
        const val URL_SEND_REQUEST_TO_REDEMPOINTS="Reward_Controller/redeemRequest"
        const val API_USER_REWARD="User_Controller/getTotalPoints"
        var gson=Gson()
    }



    @SuppressLint("CheckResult")
    override fun doLogin(mobile: String, password: String): Observable<UserData> {
        Log.d(TAG, "doLogin() called with: mobile = $mobile, password = $password")
        val params = HashMap<String, String>()
        params["password"] = password
        params["phone"] = mobile
        val result: PublishSubject<UserData> =
            PublishSubject.create<UserData>()
        var user:UserData
        RestHelper.DEFAULT_BASE_URL=BASE_URL
        RestHelper.Builder().setUrl(API_LOGIN_URL)
            .setParams(params) /*.setCallBack((code, message, restResponse)-> {
    ////        })*/.build().sendRequest().subscribe(object :Consumer<RestResponse> {

                override fun accept(restResponse: RestResponse) {
                    Log.d(TAG, "accept() called with: restResponse = $restResponse")

                    val jsonObject: JSONObject? = AppUtils.checkResponse(restResponse.code,
                        restResponse.message,
                        restResponse)
                    if (jsonObject != null) {
                        try {
                            val jsonObj =
                                jsonObject.getJSONObject(Constants.RES_OBJ_KEY)
                            user = gson.fromJson(jsonObj.toString(), UserData::class.java)
                            result.onNext(user)
                            Log.d(TAG, "accept: onnext call in if block")

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        if(!restResponse.isSucessFul()) {
                            result.onError(Throwable(restResponse.message))
                            Log.d(TAG, "accept: onnext call in else block")
                        }
                    }

                }
            })
        return result

    }

    @SuppressLint("CheckResult")
    override fun getRewardPoint(employerId: String, employeeId: String):Observable<RewardData> {
        val params = HashMap<String, String>()
        params["employer_id"] = employerId
        params["user_id"] = employeeId
        val result: PublishSubject<RewardData> =
            PublishSubject.create<RewardData>()
        RestHelper.DEFAULT_BASE_URL=BASE_URL
        RestHelper.Builder().setUrl(API_REWARD_POINT)
            .setParams(params) /*.setCallBack((code, message, restResponse)-> {
    ////        })*/.build().sendRequest().subscribe(object :Consumer<RestResponse> {

                override fun accept(restResponse: RestResponse) {
                    Log.d(TAG, "accept() called with: restResponse = $restResponse")

                    val jsonObject: JSONObject? = AppUtils.checkResponse(restResponse!!.code,
                        restResponse.message,
                        restResponse)
                    if (jsonObject != null) {
                        try {
                            val jsonary =
                                jsonObject.getJSONArray(Constants.RES_OBJ_KEY)
                            val rewarddata: RewardData =
                                gson.fromJson(jsonary.get(0).toString(),
                                    RewardData::class.java)

                            result.onNext(rewarddata)
                            Log.d(TAG, "accept: onnext call in if block")

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        if(!restResponse.isSucessFul())
                        result.onError(Throwable(restResponse.message))
                        Log.d(TAG, "accept: onnext call in else block")
                    }

                }
            })
        return result

    }

    @SuppressLint("CheckResult")
    override fun getRewardList(employeeId: String): Observable<List<UserRewardList>> {
        val params = HashMap<String, String>()
        params["user_id"] = employeeId
        val result: PublishSubject<List<UserRewardList>> =
            PublishSubject.create<List<UserRewardList>>()
        RestHelper.DEFAULT_BASE_URL=BASE_URL
        RestHelper.Builder().setUrl(API_REWARD_LIST)
            .setParams(params) /*.setCallBack((code, message, restResponse)-> {
    ////        })*/.build().sendRequest().subscribe(object :Consumer<RestResponse> {

                override fun accept(restResponse: RestResponse) {
                    Log.d(TAG, "accept() called with: restResponse = $restResponse")

                    val jsonObject: JSONObject? = AppUtils.checkResponse(restResponse!!.code,
                        restResponse.message,
                        restResponse)
                    if (jsonObject != null) {
                        try {
                            val jsonary =
                                jsonObject.getJSONArray(Constants.RES_OBJ_KEY)

                            var rewardList = gson.fromJson(jsonary.toString(),
                                Array<UserRewardList>::class.java).toList()
                            result.onNext(rewardList)
                            Log.d(TAG, "accept: onnext call in if block")

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        if(!restResponse.isSucessFul())
                            result.onError(Throwable(restResponse.message))
                        Log.d(TAG, "accept: onnext call in else block")
                    }

                }
            })
        return result
    }

    @SuppressLint("CheckResult")
    override fun getRedempionList(
        employerId: String,
        employeeId: String,
        status: String,
    ): Observable<List<Redemption>> {

        val params = HashMap<String, String>()
        params["employer_id"] = employerId
        params["user_id"] = employeeId
        params["redeem_status"]=status
        Log.d(TAG,
            "getRedempionList() called with: employerId = $employerId, employeeId = $employeeId, status = $status")
        val result: PublishSubject<List<Redemption>> =
            PublishSubject.create<List<Redemption>>()
        RestHelper.DEFAULT_BASE_URL=BASE_URL
        RestHelper.Builder().setUrl(API_REDEMPTION)
            .setParams(params) /*.setCallBack((code, message, restResponse)-> {
    ////        })*/.build().sendRequest().subscribe(object :Consumer<RestResponse> {

                override fun accept(restResponse: RestResponse) {
                    Log.d(TAG, "accept() called with: restResponse = $restResponse")

                    val jsonObject: JSONObject? = AppUtils.checkResponse(restResponse.code,
                        restResponse.message,
                        restResponse)
                    if (jsonObject != null) {
                        try {
                            val jsonary =
                                jsonObject.getJSONArray(Constants.RES_OBJ_KEY)

                            var redemptionList = gson.fromJson(jsonary.toString(),
                                Array<Redemption>::class.java).toList()
                            result.onNext(redemptionList)
                            Log.d(TAG, "accept: onnext call in if block")

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        if(!restResponse.isSucessFul())
                            result.onError(Throwable(restResponse.message))
                        Log.d(TAG, "accept: onnext call in else block")
                    }

                }
            })
        return result




    }

    @SuppressLint("CheckResult")
    override fun forgotPassword(phone: String):Observable<String> {

        val params = HashMap<String, String>()
        params["phone"] = phone

        val result: PublishSubject<String> =
            PublishSubject.create<String>()
        RestHelper.DEFAULT_BASE_URL=BASE_URL
        RestHelper.Builder().setUrl(API_FORGOT_PASSWORD)
            .setParams(params) /*.setCallBack((code, message, restResponse)-> {
    ////        })*/.build().sendRequest().subscribe(object :Consumer<RestResponse> {

                override fun accept(restResponse: RestResponse) {
                    Log.d(TAG, "accept() called with: restResponse = $restResponse")

                    val jsonObject: JSONObject? = AppUtils.checkResponse(restResponse.code,
                        restResponse.message,
                        restResponse)
                    if (jsonObject != null) {
                        try {

                            result.onNext(restResponse.message)
                            Log.d(TAG, "accept: onnext call in if block")

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        if(!restResponse.isSucessFul())
                            result.onError(Throwable(restResponse.message))
                        Log.d(TAG, "accept: onnext call in else block")
                    }

                }
            })
        return result




    }

    @SuppressLint("CheckResult")
    override fun getReddemRequest(
        jsonArray: JSONArray?,
        employerId: String,
        employeeId: String,
    ): Observable<String> {


        val params = HashMap<String, String>()
        params["items"]= jsonArray.toString()
        params["employer_id"] = employerId
        params["user_id"] = employeeId
        val result: PublishSubject<String> =
            PublishSubject.create<String>()
        RestHelper.DEFAULT_BASE_URL=BASE_URL
        RestHelper.Builder().setUrl(URL_SEND_REQUEST_TO_REDEMPOINTS)
            .setParams(params) /*.setCallBack((code, message, restResponse)-> {
    ////        })*/.build().sendRequest().subscribe(object :Consumer<RestResponse> {

                override fun accept(restResponse: RestResponse) {
                    Log.d(TAG, "accept() called with: restResponse = $restResponse")

                    val jsonObject: JSONObject? = AppUtils.checkResponse(restResponse.code,
                        restResponse.message,
                        restResponse)
                    if (jsonObject != null) {

                        Log.d(TAG, "accept: "+jsonObject.toString())

                            result.onNext(restResponse.message)

                    } else {
                        if(!restResponse.isSucessFul())
                            result.onError(Throwable(restResponse.message))
                        Log.d(TAG, "accept: onnext call in else block")
                    }

                }
            })
        return result
    }

    @SuppressLint("CheckResult")
    override fun getUserRewardPoint(
        employerId: String,
        employeeId: String,
    ): Observable<RewardDetail> {
        val params = HashMap<String, String>()
        params["employer_id"] = employerId
        params["user_id"] = employeeId
        val result: PublishSubject<RewardDetail> =
            PublishSubject.create<RewardDetail>()
        RestHelper.DEFAULT_BASE_URL=BASE_URL
        RestHelper.Builder().setUrl(API_USER_REWARD)
            .setParams(params) /*.setCallBack((code, message, restResponse)-> {
    ////        })*/.build().sendRequest().subscribe(object :Consumer<RestResponse> {

                override fun accept(restResponse: RestResponse) {
                    Log.d(TAG, "accept() called with: restResponse = $restResponse")

                    val jsonObject: JSONObject? = AppUtils.checkResponse(restResponse.code,
                        restResponse.message,
                        restResponse)
                    if (jsonObject != null) {
                        try {
                            val jsonObj =
                                jsonObject.getJSONObject(Constants.RES_OBJ_KEY)

                            var reward =
                                gson.fromJson(jsonObj.getJSONObject("reward_detail").toString(),
                                    RewardDetail::class.java)
                            result.onNext(reward)
                            Log.d(TAG, "accept: onnext call in if block")

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        if (!restResponse.isSucessFul())
                            result.onError(Throwable(restResponse.message))
                        Log.d(TAG, "accept: onnext call in else block")

                    }
                }
            })

        return result

    }

}


