package com.etech.recruitonmars.data.network.apiHelperNew

/**
 * Created by etech7 on 20/6/17.
 */

class RestConst {
    enum class RequestMethod {
        METHOD_GET,
        METHOD_POST
    }

    enum class ContentType {
        CONTENT_JSON,
        CONTENT_FORMDATA,
        CONTENT_MULTIPART
    }

    enum class ResponseCode {
        SUCCESS,
        ERROR,
        CANCEL,
        FAIL
    }

    enum class ResponseType {
        RES_TYPE_JSON,
        RES_TYPE_IMAGE
    }
}
