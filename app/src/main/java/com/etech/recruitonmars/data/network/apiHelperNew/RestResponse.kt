package com.etech.recruitonmars.data.network.apiHelperNew

import android.graphics.Bitmap
import com.etech.recruitonmars.utils.Constants
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by etech7 on 20/6/17.
 */

open class RestResponse{

    var resString: String? = null
    var error: String? = null
    var resType: RestConst.ResponseType? = null
    var bitmap: Bitmap? = null
    var code:Int = 0
    var message:String=" "
    var jsonObject:JSONObject?=null
     var jsonArray:JSONArray?=null


    fun isSucessFul():Boolean{
        if(code==Constants.SUCCESS_CODE)
        {
            return true
        }
        return false
    }


}
