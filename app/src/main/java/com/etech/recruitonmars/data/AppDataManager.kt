package com.etech.recruitonmars.data

import android.content.Context
import android.util.Log
import com.etech.recruitonmars.data.db.DbHelper
import com.etech.recruitonmars.data.network.ApiHelper
import com.etech.recruitonmars.data.network.apiHelperNew.RestResponse
import com.etech.recruitonmars.data.pref.PrefHelper
import com.etech.recruitonmars.model.*
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import org.json.JSONArray
import java.util.*

class AppDataManager(val context: Context,apiHelper: ApiHelper,prefHelper: PrefHelper,dbHelper: DbHelper):DataManager {
    var mApiHelper=apiHelper
    var mcontext=context
    var mprefHelper=prefHelper
    var mdbHelper=dbHelper
    var gson=Gson()
    private  val TAG = "AppDataManager"
    override fun isLogin(): Boolean {
        if (mprefHelper.getUserData().userId.isNullOrEmpty())
        {
            Log.d(TAG, "isLogin() called in if")
            return false
        }
        Log.d(TAG, "isLogin() called in else")

        Log.d(TAG, "user id is:"+mprefHelper.getUserData().userId.toString())

        return true

    }

    override fun saveUserData(userData: Userdetails) {

           mprefHelper.saveUserData(userData)
    }

    override fun getUserData():Userdetails {
          return mprefHelper.getUserData()
    }

    override fun saveRewardData(rewardDetail: RewardDetail) {

         mprefHelper.saveRewardData(rewardDetail)
    }

    override fun getRewardData(): RewardDetail {
       return mprefHelper.getRewardData()

    }

    override fun logout() {
       return mprefHelper.logout()
    }

    override fun doLogin(mobile: String, password: String): Observable<UserData> {
        return mApiHelper.doLogin(mobile,password).doOnNext{it->

            Log.d(TAG, "doAfterNext called with: it = $it")

            if (it != null) {
                saveUserData(it.userDetails)
                saveRewardData(it.rewardDetail)
                Log.d(TAG, "doLogin: "+it.userDetails.employerName)
                Log.d(TAG, "doLogin: "+it.rewardDetail.pendingRedeemPoint)

            }

            Log.d(TAG, "doLogin() called with: it = $it")


        }

    }

    override fun getRewardPoint(employerId: String, employeeId: String):Observable<RewardData> {
         return mApiHelper.getRewardPoint(employerId,employeeId)

    }

    override fun getRewardList(employeeId: String): Observable<List<UserRewardList>> {
        return mApiHelper.getRewardList(employeeId)
    }

    override fun getRedempionList(
        employerId: String,
        employeeId: String,
        status: String,
    ): Observable<List<Redemption>> {



   return mApiHelper.getRedempionList(employerId,employeeId,status)


    }

    override fun forgotPassword(phone: String): Observable<String> {
          return mApiHelper.forgotPassword(phone)
    }

    override fun getReddemRequest(
        jsonArray: JSONArray?,
        employerId: String,
        employeeId: String,
    ): Observable<String> {
        return mApiHelper.getReddemRequest(jsonArray,employerId,employeeId)
    }

    override fun getUserRewardPoint(
        employerId: String,
        employeeId: String,
    ): Observable<RewardDetail> {
        return mApiHelper.getUserRewardPoint(employerId,employeeId).doOnNext {
                it->
//            val rewardDetail:RewardDetail ? =
//                gson.fromJson(it.jsonObject!!.getJSONObject("reward_detail").toString(), RewardDetail::class.java)
//            Log.d(TAG, "doAfterNext called with: it = $it")

            if (it != null) {
                saveRewardData(it)
                Log.d(TAG, "getUserReward: "+it.pendingRedeemPoint)

            }

        }
    }


}
