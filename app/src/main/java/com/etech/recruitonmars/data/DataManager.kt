package com.etech.recruitonmars.data

import com.etech.recruitonmars.data.db.DbHelper
import com.etech.recruitonmars.data.network.ApiHelper
import com.etech.recruitonmars.data.pref.PrefHelper

interface DataManager:PrefHelper,DbHelper,ApiHelper {

   fun isLogin():Boolean




}