package com.etech.recruitonmars.data.pref

import android.content.Context
import android.util.Log
import com.etech.recruitonmars.model.RewardDetail
import com.etech.recruitonmars.model.UserData
import com.etech.recruitonmars.model.Userdetails

class AppPrefHelper(context:Context):PrefHelper {

    private  val TAG = "AppPrefHelper"


    val PREF_USER_ID= "user_id"
    val PREF_USER_FNAME = "fname"
    val PREF_USER_LNAME = "lname"
    val PREF_USER_STATUS = "status"
    val PREF_USER_EMAIL = "email"
    val PREF_USER_MOBILE = "mobile"
    val PREF_USER_PASSWORD = "pword"
    val PREF_USER_TYPE = "user_type"
    val PREF_EMPLOYER_ID = "employerId"
    val PREF_EMPLOYEE_ID = "employeeId"
//    val PREF_USER_PROFILEIMAGE_THUMB = "profileImageUrlThumb"
    val EMPLOYER_LOGO = "employerlogo"
    val EMPLOYER_NAME = "employername"
    val EMPLOYER_STATUS = "employerstatus"


    val BALANCE_REWARD_POINT="balancerewardpoint"
    val PENDING_REWARD_POINT="pointrewardpoint"
    val prefapp=context.getSharedPreferences("UserData",Context.MODE_PRIVATE)
    var editor=prefapp.edit()
    override fun saveUserData(userData: Userdetails) {
        editor.putString(PREF_USER_ID,userData.userId)
        editor.putString(PREF_USER_TYPE,userData.userType)
        editor.putString(PREF_EMPLOYEE_ID,userData.employeeId)
        editor.putString(PREF_EMPLOYER_ID,userData.employerId)
        editor.putString(PREF_USER_EMAIL,userData.email)
        editor.putString(PREF_USER_MOBILE,userData.mobile)
        editor.putString(PREF_USER_FNAME,userData.firstName)
        editor.putString(PREF_USER_LNAME,userData.lastName)
        editor.putString(PREF_USER_PASSWORD,userData.password)
        editor.putString(PREF_USER_STATUS,userData.userStatus)
        editor.putString(EMPLOYER_LOGO,userData.employerLogo)
        editor.putString(PREF_USER_ID,userData.userId)
        editor.putString(EMPLOYER_NAME,userData.employerName)
        editor.putString(EMPLOYER_STATUS,userData.userStatus)
        editor.commit()


    }

    override fun getUserData(): Userdetails {
        var userdetails=Userdetails()
       userdetails.email= prefapp.getString(PREF_USER_ID,userdetails.userId)
        userdetails.userType=prefapp.getString(PREF_USER_TYPE,userdetails.userType)
        userdetails.employeeId=prefapp.getString(PREF_EMPLOYEE_ID,userdetails.employeeId)
        userdetails.employerId=prefapp.getString(PREF_EMPLOYER_ID,userdetails.employerId)
        userdetails.email= prefapp.getString(PREF_USER_EMAIL,userdetails.email)
        userdetails.mobile=prefapp.getString(PREF_USER_MOBILE,userdetails.mobile)
        userdetails.firstName=prefapp.getString(PREF_USER_FNAME,userdetails.firstName)
        userdetails.lastName=prefapp.getString(PREF_USER_LNAME,userdetails.lastName)
        userdetails.password=prefapp.getString(PREF_USER_PASSWORD,userdetails.password)
        userdetails.userStatus =prefapp.getString(PREF_USER_STATUS,userdetails.userStatus)
        userdetails.employerLogo =prefapp.getString(EMPLOYER_LOGO,userdetails.employerLogo)
        userdetails.userId =prefapp.getString(PREF_USER_ID,userdetails.userId)
        userdetails.employerName =prefapp.getString(EMPLOYER_NAME,userdetails.employerName)
        userdetails.userStatus =prefapp.getString(EMPLOYER_STATUS,userdetails.userStatus)
        return userdetails


    }

    override fun saveRewardData(rewardDetail: RewardDetail) {
        editor.putString(BALANCE_REWARD_POINT,rewardDetail.balanceRewardPoint)
        editor.putString(PENDING_REWARD_POINT,rewardDetail.pendingRedeemPoint)
        editor.commit()

    }

    override fun getRewardData(): RewardDetail {

        var rewardDetail=RewardDetail()
        rewardDetail.balanceRewardPoint=prefapp.getString(BALANCE_REWARD_POINT,rewardDetail.balanceRewardPoint)
        rewardDetail.pendingRedeemPoint=prefapp.getString(PENDING_REWARD_POINT,rewardDetail.pendingRedeemPoint)
        Log.d(TAG, "getRewardData() called"+rewardDetail.balanceRewardPoint)
        Log.d(TAG, "getRewardData: "+rewardDetail.pendingRedeemPoint)
        return rewardDetail


    }

    override fun logout() {
        editor.clear().commit()
    }


}