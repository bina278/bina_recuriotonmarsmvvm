package com.etech.recruitonmars.data.network.apiHelperNew


import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.webkit.MimeTypeMap
import androidx.annotation.NonNull
import com.etech.recruitonmars.utils.AppUtils
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*


import java.io.File
import java.util.ArrayList
import java.util.HashMap

class RestClient(private val restRequest: RestRequest, private val restClientListener: RestClientListener) {
    @Suppress("PrivatePropertyName")
    private val TAG = "RestClient"
    private var call: Call<String>? = null
    internal fun execute() {
        Log.d(this.TAG, "execute()==" + restRequest.toString())
//        val res = RestResponse(restRequest)
         var res=RestResponse()
        val callFactory = getRetrofit(restRequest.baseUrl).create(CallFactory::class.java)
        when (restRequest.reqMethod) {
            RestConst.RequestMethod.METHOD_GET -> {
                call = callFactory.getGetCall(
                    restRequest.reqUrl,
                    restRequest.header ?: HashMap(),
                    restRequest.params ?: HashMap()
                )
            }
            RestConst.RequestMethod.METHOD_POST -> {
                when (restRequest.contentType) {
                    RestConst.ContentType.CONTENT_FORMDATA -> {
                        call = callFactory.getPostCallWithParams(
                            restRequest.reqUrl,
                            restRequest.header ?: HashMap(),
                            restRequest.params ?: HashMap()
                        )
                    }
                    RestConst.ContentType.CONTENT_JSON -> {
                        call = callFactory.getPostCallWithJsonBody(
                            restRequest.reqUrl,
                            restRequest.header ?: HashMap(),
                            RequestBody.create("Application/json".toMediaTypeOrNull(), restRequest.jsonObject.toString())
                        )
                    }
                    RestConst.ContentType.CONTENT_MULTIPART -> {
                        val arrListPart = ArrayList<MultipartBody.Part>()
                        if (restRequest.attachments != null) {
                            for ((key, values) in restRequest.attachments) {
                                for (filePath in values) {
                                    val file = File(filePath)
                                    val requestBody =
                                        RequestBody.create("Application/json".toMediaTypeOrNull(), restRequest.jsonObject.toString())
                                    arrListPart.add(MultipartBody.Part.createFormData(key, file.name, requestBody))

                                }
                            }
                        }
                        val parts = arrListPart.toTypedArray()
                        call = callFactory.getPostCallWithFileUpload(
                            restRequest.reqUrl,
                            restRequest.header ?: HashMap(),
                            restRequest.params ?: HashMap(),
                            parts
                        )
                    }
                }
            }
        }
        if (call != null) {
            if (AppUtils.isConnectingToInternet()) {
                call!!.enqueue(object : Callback<String> {
                    override fun onResponse(@NonNull call: Call<String>, @NonNull response: Response<String>) {
                        val statusCode = response.code()
                        val responseString = response.body()
                        Log.d(TAG, "onResponce for" + restRequest.reqUrl)
                        Log.d(TAG, "responseCode=$statusCode")
                        Log.d(TAG, "response=$responseString")

                        if (response.raw().body != null && response.raw().body!!.contentType() != null && "image/*".equals(
                                response.raw().body!!.contentType().toString(),
                                ignoreCase = true
                            )
                        ) {
                            try {
                                var bitmap: Bitmap? = null
                                if (response.body() != null) {
                                    val bytes = response.body()!!.toByteArray()
                                    val options = BitmapFactory.Options()
                                    res.bitmap= BitmapFactory.decodeByteArray(bytes, 0, bytes.size, options)
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            res.resType = RestConst.ResponseType.RES_TYPE_IMAGE


                        } else {
                            res.resType = RestConst.ResponseType.RES_TYPE_JSON
                        }

                        if (response.isSuccessful) {
                            res.resString = responseString
                            restClientListener.onRequestComplete(RestConst.ResponseCode.SUCCESS, res)
                        } else {
                            try {

                                if (response.errorBody() != null) {
                                    res.error = response.errorBody()!!.string()
                                } else {
                                    throw Exception("empty error string in response")
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                                res.error = UNKNOWN_ERR
                            }

                            restClientListener.onRequestComplete(RestConst.ResponseCode.FAIL, res)
                        }
                    }

                    override fun onFailure(@NonNull call: Call<String>, @NonNull t: Throwable) {
                        Log.d("onFailure", "for" + restRequest.reqUrl)
                        Log.d("onFailure", "cause" + t.toString())
                        /*if (t.toString().equalsIgnoreCase("java.net.SocketTimeoutException")) {
                            res.setError(INTERNAL_SERVER_ERR);
                        } else {
                            res.setError(t.toString());
                        }*/
                        res.error = t.toString()
                        if (call.isCanceled) {
                            restClientListener.onRequestComplete(RestConst.ResponseCode.CANCEL, res)
                        } else {
                            restClientListener.onRequestComplete(RestConst.ResponseCode.FAIL, res)
                        }
                    }
                })
            } else {
                Log.d(TAG, "onResponce for" + restRequest.reqUrl)
                Log.d(TAG, "No internet detected")
                res.error = NO_INTERNET_ERR
                restClientListener.onRequestComplete(RestConst.ResponseCode.ERROR, res)
            }
        } else {
            Log.d(TAG, "onResponce for" + restRequest.reqUrl)
            Log.d(TAG, "Suitable call not found")
            res.error = UNABLE_TO_PROCESS_CALL
            restClientListener.onRequestComplete(RestConst.ResponseCode.ERROR, res)
        }
    }

    internal fun cancelRequest() {
        if (call != null)
            call!!.cancel()
    }


    private fun getRetrofit(baseUrl: String): Retrofit {
        if (retrofit == null || oldBaseUrl == null || oldBaseUrl != baseUrl) {
            oldBaseUrl = baseUrl
            val client = OkHttpClient.Builder()
                //                      .connectTimeout(30, TimeUnit.SECONDS)
                //                      .readTimeout(30, TimeUnit.SECONDS)
                .build()
            val builder = Retrofit.Builder()
            builder.baseUrl(baseUrl)
            builder.client(client)
            builder.addConverterFactory(ScalarsConverterFactory.create())
            retrofit = builder.build()
        }
        return retrofit!!
    }

    private interface CallFactory {
        @GET
        fun getGetCall(@Url url: String?, @HeaderMap headers: Map<String, String>?, @QueryMap param: Map<String, String>?): Call<String>

        @POST
        @FormUrlEncoded
        fun getPostCallWithParams(@Url url: String?, @HeaderMap headers: Map<String, String>?, @FieldMap param: Map<String, String>?): Call<String>

        @POST
        fun getPostCallWithJsonBody(@Url url: String?, @HeaderMap headers: Map<String, String>?, @Body `object`: RequestBody): Call<String>

        @Multipart
        @POST
        fun getPostCallWithFileUpload(@Url url: String?, @HeaderMap headers: Map<String, String>?, @PartMap param: Map<String, String>?, @Part files: Array<MultipartBody.Part>): Call<String>


    }

    interface RestClientListener {
        fun  onRequestComplete(responseCode: RestConst.ResponseCode, restResponse: RestResponse)
    }

    private fun getMimeType(file: String): String {
        val extension = MimeTypeMap.getFileExtensionFromUrl(file)

        var mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        if (mimeType == null)
            mimeType = "*/*"
        return mimeType
    }

    companion object {

        private const val UNKNOWN_ERR = "Unknown Error Occurred"
        private const val NO_INTERNET_ERR = "No internet available"
        private const val UNABLE_TO_PROCESS_CALL = "Unable to process request"
        private var retrofit: Retrofit? = null
        private var oldBaseUrl: String? = null
    }

}
