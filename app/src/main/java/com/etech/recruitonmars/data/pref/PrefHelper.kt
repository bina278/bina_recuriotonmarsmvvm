package com.etech.recruitonmars.data.pref

import com.etech.recruitonmars.model.RewardDetail
import com.etech.recruitonmars.model.UserData
import com.etech.recruitonmars.model.Userdetails

interface PrefHelper {

     fun saveUserData(userData: Userdetails)

     fun getUserData():Userdetails

     fun saveRewardData(rewardDetail: RewardDetail)

     fun getRewardData():RewardDetail

     fun logout()



}