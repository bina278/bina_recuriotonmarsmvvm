package com.etech.recruitonmars.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.etech.recruitonmars.R
import com.etech.recruitonmars.callback.OnRecylerViewClickListener
import com.etech.recruitonmars.databinding.PointRewarsItemLayoutBinding
import com.etech.recruitonmars.databinding.RedemptionItemBinding
import com.etech.recruitonmars.model.Redemption
import com.etech.recruitonmars.model.RewardList

open class RedemptionAdapter(context: Context, redemption: List<Redemption>, onItemClick: OnRecylerViewClickListener<Redemption>):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    var context: Context
    var arrayList: List<Redemption>
    var onRecyclerViewItemClickListener: OnRecylerViewClickListener<Redemption>

    init {
        this.context = context
        this.arrayList = redemption
        this.onRecyclerViewItemClickListener = onItemClick
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.redemption_item,
            parent,
            false))


    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val redemption: Redemption = arrayList.get(position)
            if (!arrayList.isEmpty()) {
                (holder).bind(position,
                    redemption, onRecyclerViewItemClickListener)
            }
        }


    }

    override fun getItemCount(): Int {
        return arrayList.size

    }


    class ViewHolder(binding: RedemptionItemBinding) :
        RecyclerView.ViewHolder(binding.getRoot()) {
        var binding: RedemptionItemBinding

        init {
            this.binding = binding
        }

        fun bind(
            position: Int,
            redemption: Redemption,
            onItemClick: OnRecylerViewClickListener<Redemption>
        ) {
            binding.reward.text = redemption.reward
            binding.pointValue.text = redemption.redeemPoint
//            binding.rewardCode.text = rewardList.rewardCode
//            binding.rewardCard.setOnClickListener { v ->
//                onItemClick.onClick(position, rewardList)
//
//            }





        }


    }
}
