package com.etech.recruitonmars.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.etech.recruitonmars.R
import com.etech.recruitonmars.callback.OnRecylerViewClickListener
import com.etech.recruitonmars.databinding.PointRewarsItemLayoutBinding
import com.etech.recruitonmars.databinding.RewardListItemBinding
import com.etech.recruitonmars.model.RewardList
import com.etech.recruitonmars.model.UserRewardList

open class RewardListAdapter(context: Context, rewardlist: List<UserRewardList>, onItemClick: OnRecylerViewClickListener<UserRewardList>):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    var context: Context
    var arrayList: List<UserRewardList>
    var onRecyclerViewItemClickListener: OnRecylerViewClickListener<UserRewardList>

    init {
        this.context = context
        this.arrayList = rewardlist
        this.onRecyclerViewItemClickListener = onItemClick
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.reward_list_item,
            parent,
            false))


    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val rewardList: UserRewardList = arrayList.get(position)
            if (!arrayList.isEmpty()) {
                (holder).bind(position,
                    rewardList, onRecyclerViewItemClickListener)
            }
        }


    }

    override fun getItemCount(): Int {
        return arrayList.size

    }


    class ViewHolder(binding: RewardListItemBinding) : RecyclerView.ViewHolder(binding.getRoot()) {
        var binding: RewardListItemBinding

        init {
            this.binding = binding
        }

        fun bind(
            position: Int,
            reward: UserRewardList,
            onItemClick: OnRecylerViewClickListener<UserRewardList>
        ) {
            binding.rewardName.text = reward.rewardName
            binding.point.text = reward.rewardPoint
            binding.itemcount.text=reward.quantity.toString()
            if(reward.quantity==0)
            {
                binding.llBtnPlusMinus.visibility=View.GONE
                binding.addToCart.visibility=View.VISIBLE
            }
            else
            {
                binding.llBtnPlusMinus.visibility=View.VISIBLE
                binding.addToCart.visibility=View.GONE

            }


            binding.addToCart.setOnClickListener { v ->
                onItemClick.onClick(position, reward,binding.addToCart,OnRecylerViewClickListener.Btn_AddToCart)

            }
            binding.btnminus.setOnClickListener{
                v->
                onItemClick.onClick(position,reward,binding.btnminus,OnRecylerViewClickListener.Btn_Minus)
            }
            binding.btnplus.setOnClickListener{
                    v->
                onItemClick.onClick(position,reward,binding.btnplus,OnRecylerViewClickListener.Btn_Plus)
            }



        }


    }
}