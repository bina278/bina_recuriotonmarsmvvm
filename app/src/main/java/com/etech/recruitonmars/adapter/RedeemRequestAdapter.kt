package com.etech.recruitonmars.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.etech.recruitonmars.R
import com.etech.recruitonmars.callback.OnRecylerViewClickListener
import com.etech.recruitonmars.databinding.RedemptionItemBinding
import com.etech.recruitonmars.databinding.RequestedRewardsItemBinding
import com.etech.recruitonmars.model.Redemption
import com.etech.recruitonmars.model.UserRewardList

open class RedeemRequestAdapter (context: Context, reward: List<UserRewardList>, onItemClick: OnRecylerViewClickListener<UserRewardList>):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var context: Context
    var arrayList: List<UserRewardList>
    var onRecyclerViewItemClickListener: OnRecylerViewClickListener<UserRewardList>

    init {
        this.context = context
        this.arrayList = reward
        this.onRecyclerViewItemClickListener = onItemClick
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.requested_rewards_item,
            parent,
            false))


    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val rewardList: UserRewardList = arrayList.get(position)
            if (!arrayList.isEmpty()) {
                (holder).bind(position,
                    rewardList, onRecyclerViewItemClickListener)
            }
        }


    }

    override fun getItemCount(): Int {
        return arrayList.size

    }


    class ViewHolder(binding: RequestedRewardsItemBinding) :
        RecyclerView.ViewHolder(binding.getRoot()) {
        var binding: RequestedRewardsItemBinding
        var totalPoint:Int=0
        init {
            this.binding = binding
        }

        fun bind(
            position: Int,
            reward: UserRewardList,
            onItemClick: OnRecylerViewClickListener<UserRewardList>
        ) {
            binding.rewardName.text = reward.rewardName
            binding.point.text = reward.rewardPoint
            binding.itemcount.text=reward.quantity.toString()
            binding.itemPoint.text=(reward.rewardPoint!!.toInt() * reward.quantity).toString()
            binding.btnplus.setOnClickListener { v->
                onItemClick.onClick(position,reward,binding.btnplus,OnRecylerViewClickListener.Btn_Plus)
            }
            binding.btnminus.setOnClickListener { v->
                onItemClick.onClick(position,reward,binding.btnminus,OnRecylerViewClickListener.Btn_Minus)
            }




        }


    }
}
