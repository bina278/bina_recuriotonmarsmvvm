package com.etech.recruitonmars.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.etech.recruitonmars.R
import com.etech.recruitonmars.callback.OnRecylerViewClickListener
import com.etech.recruitonmars.databinding.PointRewarsItemLayoutBinding
import com.etech.recruitonmars.model.RewardList

open class PointRewarsAdapter(context: Context,rewardlist: List<RewardList>,onItemClick: OnRecylerViewClickListener<RewardList>):RecyclerView.Adapter<RecyclerView.ViewHolder>() {


      var context: Context
      var arrayList: List<RewardList>
    var onRecyclerViewItemClickListener: OnRecylerViewClickListener<RewardList>

    init {
        this.context = context
        this.arrayList = rewardlist
        this.onRecyclerViewItemClickListener = onItemClick
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.point_rewars_item_layout,
            parent,
            false))


    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val rewardList: RewardList = arrayList.get(position)
            if (!arrayList.isEmpty()) {
                (holder).bind(position,
                    rewardList,onRecyclerViewItemClickListener)
            }
        }


    }

    override fun getItemCount(): Int {
        return arrayList.size

    }


    class ViewHolder(binding: PointRewarsItemLayoutBinding) : RecyclerView.ViewHolder(binding.getRoot()) {
        var binding: PointRewarsItemLayoutBinding
        init {
            this.binding = binding
        }
        fun bind(position: Int, rewardList: RewardList,onItemClick: OnRecylerViewClickListener<RewardList>) {
            binding.createdate.text = rewardList.createDate
            binding.rewardPoint.text = rewardList.rewardPoint
            binding.rewardCode.text = rewardList.rewardCode
            binding.rewardCard.setOnClickListener { v ->
                onItemClick.onClick(position,rewardList,null,null)

            }
        }



    }
}