package com.etech.recruitonmars.utils

class Constants {

    companion object {
        const val PRIVACY_POLICY_URL:String="https://policies.google.com/privacy?hl=en&gl=ZZ"
        const val SUCCESS_CODE = 1
        const val FAIL_CODE = 0
        const val CANCEL_CODE = 2
        const val FAIL_INTERNET_CODE = 3
        const val RES_MSG_KEY = "res_message"
        const val RES_CODE_KEY = "res_code"
        const val RES_OBJ_KEY = "res_object"
        const val SOMETHING_WENT_WRONG = "Something went wrong."
        lateinit var REDEEM_STATUS:String
    }
}