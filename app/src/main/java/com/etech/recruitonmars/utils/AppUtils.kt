package com.etech.recruitonmars.utils

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.core.content.ContextCompat
import com.etech.recruitonmars.R
import com.etech.recruitonmars.app.MyApplication
import com.etech.recruitonmars.data.network.apiHelperNew.RestResponse
//import com.etech.recruitonmars.model.Userdetails
import org.json.JSONObject

class AppUtils {

    companion object {
        private  val TAG = "AppUtils"

        fun isConnectingToInternet(context: Context = MyApplication.appContext): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = cm.activeNetworkInfo
            return networkInfo?.isConnected ?: false
        }


        fun setStatusbarColor(activity: Activity) {
            try {
                val window = activity.window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.statusBarColor = ContextCompat.getColor(activity, R.color.colorPrimary);
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        fun setVisibility(view: View?, visibility: Int) {
            if (view == null) {
                Log.i(AppUtils.TAG, "view is null")
                return
            }
            if (view.visibility != visibility) view.visibility = visibility
            Log.i(AppUtils.TAG, "view is not null")
        }
        fun checkResponse(
            code: Int?, message: String?, restResponse: RestResponse,
        ): JSONObject? {
            if (code == Constants.SUCCESS_CODE) {
                try {
                    val res = JSONObject(restResponse.resString)
                    val flag: String = res.getString(Constants.RES_CODE_KEY)
                    return if (flag.toInt() == 1) {
                        res
                    } else {
                        null
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
            return null
        }


    }
}