package com.etech.recruitonmars.utils

import android.app.Dialog
import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.etech.recruitonmars.R

class CustomAlert {
    companion object {
        fun showAlert(context: Context, title: String?, message: String?) {
            showAlert(
                context,
                title,
                message,
                null,
                null,
                null,
                null,
                context.resources.getString(R.string.alt_ok_btn),
                null
            )
        }

//            fun showAlert(
//                context: Context?,
//                title: String?,
//                message: String?,
//                btnText1: String?,
//                btn1listener: View.OnClickListener?,
//                btnText2: String?,
//                btn2clickListener: View.OnClickListener?,
//                btnText: String?,
//                btnClickListener: View.OnClickListener?
//
//            ){
//                showAlert(context,title,message,btnText1, btn1listener, btnText2, btn2clickListener, btnText, btnClickListener)
//            }


        fun showAlert(
            context: Context?,
            title: String?,
            message: String?,
            btnText1: String?,
            btn1listener: View.OnClickListener?,
            btnText2: String?,
            btn2clickListener: View.OnClickListener?,
            btnText: String?,
            btnClickListener: View.OnClickListener?

        ) {
            val dialog = Dialog(context!!)
            dialog.setContentView(R.layout.custome_alert)
            val Title = dialog.findViewById<TextView>(R.id.title)
            val Message = dialog.findViewById<TextView>(R.id.text)
            val btn1 = dialog.findViewById<Button>(R.id.ok_btn)
            val btn2 = dialog.findViewById<Button>(R.id.cancel_btn)
            val btn = dialog.findViewById<Button>(R.id.btn)
            Title.text = title
            Message.text = message
            if (btnText1 != null) {
                btn1.text = btnText1
            }
            if (btn1listener != null) {
                btn1.setOnClickListener { v: View? ->
                    btn1listener.onClick(
                        btn1
                    )
                }
            } else {
                btn1.setOnClickListener { v: View? -> dialog.dismiss() }
            }
            if (btnText2 != null) {
                btn2.text = btnText2
            }
            if (btn2clickListener != null) {
                btn2.setOnClickListener { v: View? ->
                    btn2clickListener.onClick(
                        btn2
                    )
                }
            } else {
                btn2.setOnClickListener { v: View? -> dialog.dismiss() }
            }
            if (btnText != null) {
                btn.visibility = View.VISIBLE
                btn.setText(btnText)


            }
            if (btnClickListener != null) {
                btn.setOnClickListener { v: View? ->
                    btnClickListener.onClick(btn)

                }
            } else {

                btn.setOnClickListener { v: View? -> dialog.dismiss() }

            }


            dialog.show()
        }

        fun showRewardPointDiolog(
            context: Context,
            rewardCode: String?,
            rewardName: String?,
            fullname: String?,
            rewardPoint: String?,
            createdate: String?
        ) {

            val dialog = Dialog(context!!)
            dialog.setContentView(R.layout.reward_point_dialog)
            val rewardType = dialog.findViewById<TextView>(R.id.rewardCode)
            val txtrewardName = dialog.findViewById<TextView>(R.id.rewardname)
            val txtcandidateName = dialog.findViewById<TextView>(R.id.candidatename)
            val txtrewardPoint = dialog.findViewById<TextView>(R.id.rewardPoint)
            val txtcreatedate = dialog.findViewById<TextView>(R.id.createdate)
            val okBtn = dialog.findViewById<Button>(R.id.ok_btn)
            rewardType.text = rewardCode
            txtrewardName.text = rewardName
            txtcandidateName.text = fullname
            txtrewardPoint.text = rewardPoint
            txtcreatedate.text = createdate
            okBtn.setOnClickListener { v: View? ->
                dialog.dismiss()
            }

            dialog.show()
        }


    }
}

