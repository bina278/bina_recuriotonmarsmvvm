package com.etech.recruitonmars.callback

import android.view.View
import java.lang.NullPointerException

public interface OnRecylerViewClickListener<V> {

        companion object{
              const  val Btn_Plus=1
              const val  Btn_Minus=2
              const val Btn_AddToCart=3

        }


        fun onLastItemReached();

        fun onClick(position: Int,model: V,view: View?,viewType : Int?)


}