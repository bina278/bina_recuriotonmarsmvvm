package com.etech.recruitonmars.fragments.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etech.recruitonmars.app.MyApplication
import com.etech.recruitonmars.base.BaseActivity
import com.etech.recruitonmars.base.BaseViewModel
import com.etech.recruitonmars.data.DataManager
import com.etech.recruitonmars.data.network.apiHelperNew.RestResponse
import com.etech.recruitonmars.model.Redemption
import com.etech.recruitonmars.model.RewardDetail
import com.etech.recruitonmars.model.UserData
import com.google.gson.Gson
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class HomeViewModel:BaseViewModel() {

    var rewarsDetails=MutableLiveData<RewardDetail>()
    private  val TAG = "HomeViewModel"
    var gson=Gson()
    fun getRewardData()
    {
          Log.d(TAG, "getRewardData() called")
        val employerId= MyApplication.datamanger.getUserData().employerId
        val employeeId= MyApplication.datamanger.getUserData().employeeId
        if(employeeId!=null && employerId!=null) {
            MyApplication.datamanger.getUserRewardPoint(employerId, employeeId)
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(
                    Schedulers.io()).subscribe(
                    object : Observer<RewardDetail> {

                        override fun onSubscribe(d: Disposable) {}
                        override fun onNext(response: RewardDetail) {
                            Log.d(TAG, "onNext() called with: response = $response")
                            loadingSucess()


                                rewarsDetails.value=response


                        }

                        override fun onError(e: Throwable) {
                            loadingFail(e.message)
                        }
                        override fun onComplete() {
                            Log.d(TAG, "onComplete() called")
                        }
                    })
        }






    }











}