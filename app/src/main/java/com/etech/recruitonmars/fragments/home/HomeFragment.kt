package com.etech.recruitonmars.fragments.home

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etech.recruitonmars.R
import com.etech.recruitonmars.activities.dashboard.HomeActivity
import com.etech.recruitonmars.base.BaseFragment
import com.etech.recruitonmars.databinding.FragmentHomeBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val TAG = "HomeFragment"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : BaseFragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var binding:FragmentHomeBinding
    lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        homeViewModel=ViewModelProvider(this).get(HomeViewModel::class.java)
        setUpViewModel(homeViewModel)
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_home,container,false)
        initUi()
        return binding.root
    }


    fun initUi()
    {
        homeViewModel.getRewardData()
        homeViewModel.rewarsDetails.observe(requireActivity(), Observer { t->


            Log.d(TAG, "initUi: "+t.balanceRewardPoint)
            Log.d(TAG, "initUi: "+t.pendingRedeemPoint)
            binding.currentPointBalanceCircular.text=t.balanceRewardPoint
            binding.pendingValue.text=t.pendingRedeemPoint
            Log.d(TAG, "initUi() called with: t = ${t.pendingRedeemPoint}")

        })
        binding.viewPointRewarded.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                (activity as HomeActivity).pointRewardedClicked()

            }

        })
        binding.viewRedemption.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                (activity as HomeActivity).redemptionClicked()


            }

        })





    }

    override fun onResume() {
        super.onResume()
        homeViewModel.getRewardData()
    }
}