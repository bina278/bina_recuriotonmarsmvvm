package com.etech.recruitonmars.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.etech.recruitonmars.databinding.HeaderBinding

open class BaseFragment:Fragment() {
    lateinit var baseActivity:BaseActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        baseActivity = activity as BaseActivity

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        baseActivity= activity as BaseActivity

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    open fun setUpViewModel(baseViewModel: BaseViewModel) {
        baseActivity.setUpViewModel(baseViewModel)
    }


    open fun showError(message: String) {
        baseActivity.showError(message)
    }

    open fun showProgressBar() {
        baseActivity.showProgressBar()
    }

    open fun hideProgressBar() {
        baseActivity.hideProgressBar()
    }

    open fun showAlert(contecx:Context,title:String,msg:String)
    {
        baseActivity.showAlert(contecx,title,msg)
    }







}