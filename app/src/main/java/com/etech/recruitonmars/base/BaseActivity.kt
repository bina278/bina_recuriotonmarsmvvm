package com.etech.recruitonmars.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.etech.recruitonmars.R
import com.etech.recruitonmars.utils.CustomAlert
import com.etech.recruitonmars.databinding.HeaderBinding
import com.etech.recruitonmars.databinding.LayoutExtraBinding
import com.etech.recruitonmars.utils.AppUtils

open class BaseActivity: AppCompatActivity() {

    lateinit var baseviewModel:BaseViewModel
    lateinit var baseFragment: BaseFragment
    lateinit var layoutExtraBinding: LayoutExtraBinding
    lateinit var header: HeaderBinding
     var toolBar:Toolbar? = null
    var boolean:Boolean=false
    var count:Int=0;
    companion object {
        val TAG = "BaseActivity"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    fun setUpViewModel(mbaseViewModel: BaseViewModel)
    {
        this.baseviewModel=mbaseViewModel
        observeDataChanges()

    }
    fun observeDataChanges()
    {
        baseviewModel.isShowProgressBar.observe(this, Observer { t ->

            if(t)
            {
              showProgressBar()
            }
            else
            {
                hideProgressBar()
            }

        })


        baseviewModel.isShowError.observe(this,Observer{
               t ->
          showError(t)

        })

    }
    fun isLoggedIn():Boolean
    {
        Log.d(TAG, "isLoggedIn() called")
        return baseviewModel.isLogin()
    }

    fun showProgressBar()
    {
        AppUtils.setVisibility(layoutExtraBinding.progressBar,View.VISIBLE)
    }
    fun hideProgressBar()
    {
        AppUtils.setVisibility(layoutExtraBinding.progressBar,View.GONE)

    }

    fun showError(message: String)
    {
        Log.d(TAG, "showError() called with: message = $message")
        showAlert(this@BaseActivity, "Alert", message)

    }

    open fun addFragment(fragment: BaseFragment, isClearStack: Boolean, isAddToStack: Boolean) {
//        Log.d(BaseActivity.TAG, fragment.getName())
//        val tag: String = fragment.tag + count
        baseFragment = fragment
        if (isClearStack) {
            clearFragmentStack()
        }
        val transaction = supportFragmentManager
            .beginTransaction()
//        if (isAddToStack) transaction.addToBackStack(tag)
        transaction.replace(R.id.relativeLayout, fragment)
        Log.d(
            BaseActivity.TAG,
            "addFragment() called with: fragment = [$fragment], isClearStack = [$isClearStack], isAddToStack = [$isAddToStack]"
        )
        transaction.commit()
        count = count + 1
    }

    open fun clearFragmentStack() {
        for (i in 0 until supportFragmentManager.backStackEntryCount) {
            val name = supportFragmentManager.getBackStackEntryAt(0).name
            supportFragmentManager.popBackStack(name, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            break
        }
    }
    open fun setUpview(
        extraBinding: LayoutExtraBinding,
        header: HeaderBinding,
        toolBar:Toolbar?,
        title: String,
        isCenter:Boolean,
        isBackEnable: Boolean
        ){
        this.layoutExtraBinding = extraBinding
        this.header = header
        this.toolBar=toolBar
        if ( toolBar!= null) {
            setSupportActionBar(toolBar)
//            supportActionBar!!.title = title
            Log.d(BaseActivity.TAG, "title set$title")
            if (isBackEnable) {
                if (supportActionBar != null) {
                    supportActionBar!!.setDisplayHomeAsUpEnabled(true)
                    toolBar.setNavigationIcon(R.drawable.ic_arrow_back)
                    supportActionBar!!.setDisplayShowHomeEnabled(true)
                }
                    toolBar.setNavigationOnClickListener(
                    View.OnClickListener { view: View? -> onBackPressed() }
                )
            }
        }
        if(isCenter)
        {
            header.headerName.setText(title)
            supportActionBar!!.setDisplayShowTitleEnabled(false)
        }
    }
    open fun setTitle(header: HeaderBinding, title: String?) {
        header.headerName.setText(title)
    }
    open fun showMessage(context: Context?, message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    open fun showAlert(context: Context, title: String, message: String) {
        Log.d(
            TAG,
            "showAlert() called with: context = [$context], title = [$title], message = [$message]"
        )
        CustomAlert.showAlert(context, title, message)
    }

    open fun showAlert(context: Context,title: String,message: String,btn1Text:String,btn1ClickListener: View.OnClickListener?,btn2Text:String,btn2ClickListener: View.OnClickListener?)
    {
        CustomAlert.showAlert(context,title,message,btn1Text,btn1ClickListener,btn2Text,btn2ClickListener,null,null)
    }


    open fun addToCartText(): TextView
    {
        return header.txtAddTocart
    }
    open fun getCart():View{
        return header.cartImage
    }



}