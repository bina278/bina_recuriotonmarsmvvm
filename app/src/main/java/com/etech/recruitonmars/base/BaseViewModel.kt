package com.etech.recruitonmars.base

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etech.recruitonmars.app.MyApplication
import com.etech.recruitonmars.data.network.apiHelperNew.RestResponse

open class BaseViewModel:ViewModel() {
    private  val TAG = "BaseViewModel"
    val isShowProgressBar=MutableLiveData<Boolean>();
    val isShowConnectionLostView=MutableLiveData<Boolean>();
    val isShowError=MutableLiveData<String>()
    val isLogin=MutableLiveData<Boolean>()

    fun isLogin():Boolean{
        if(MyApplication.datamanger.isLogin())
        {
            Log.d(TAG, "isLogin() called")
            return true
        }
        return false
    }


    fun  startLoading()
    {
        Log.d(TAG, "startLoading() called")
         isShowProgressBar.value=true
//       isShowConnectionLostView.value=false
    }
    fun loadingSucess()
    {
        Log.d(TAG, "loadingSucess() called")
        isShowProgressBar.value=false
//        isShowConnectionLostView.value=false
    }
    fun loadingFail(message: String?)
    {
        Log.d(TAG, "loadingFail() called with: restResponse = $message")
        isShowProgressBar.value=false
        isShowError.value=message

    }






}