package com.etech.recruitonmars.app

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDexApplication
import com.etech.recruitonmars.data.AppDataManager
import com.etech.recruitonmars.data.DataManager
import com.etech.recruitonmars.data.db.AppDbHelper
import com.etech.recruitonmars.data.network.AppApihelper
import com.etech.recruitonmars.data.pref.AppPrefHelper

class MyApplication: MultiDexApplication(){
        companion object {
            lateinit var datamanger: DataManager
            lateinit var appContext: Context
        }

        override fun onCreate() {
            super.onCreate()
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
            appContext = applicationContext
            datamanger = AppDataManager(applicationContext, AppApihelper(),AppPrefHelper(appContext), AppDbHelper())
            /*CalligraphyConfig.initDefault(
                    CalligraphyConfig.Builder()
                            .setDefaultFontPath(applicationContext.getString(R.string.font_regular))
                            .setFontAttrId(R.attr.fontPath)
                            .build()
            )
    */

        }
    }

